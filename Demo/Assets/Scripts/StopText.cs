﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StopText : MonoBehaviour {

    Text text;

	void Start () {
	
	}
	
	void Update()
    {
        text = GetComponent<Text>();

        switch (PlayerMovement.orderState)
        {
            case PlayerMovement.OrderState.STOP:
                text.enabled = true;
                break;

            default:
                text.enabled = false;
                break;
        }
	}
}
