﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AllyDirector : MonoBehaviour {

    public static List<Ally> allAllies = new List<Ally>();

    public static float riskCoef = 10.0f;
    public static PlayerMovement player = null;
    public static Node playerNode = null;
    public static Node oldPlayerNode = null;
    public static int counter = 0;

    public GameObject ally;
    public GameObject playerObject;
    public static int nbrAlliesS = 5;
    public int nbrAllies;
    public Vector3 spawnValues;
    public static float maxHp = 50;
    public static int attack = 10;
    public static int randStart = 0;

	void Start ()
    {
        nbrAllies = nbrAlliesS;
        SpawnAllies();
	}

    void SpawnAllies()
    {
        randStart = Random.Range(0, 4);
        if (randStart == 3)
        {
            /*Node tmp = Graph.getExit(1);
            if (tmp)
                tmp.isExit = true;*/
            spawnValues = new Vector3(50f, 0.9f, 38f);
        }
        else if (randStart == 2)
        {
            Node tmp = Graph.getExit(2);
            if (tmp)
                tmp.isExit = true;
            spawnValues = new Vector3(-57f, 0.9f, 46f);
        }
        else if (randStart == 1)
        {
            Node tmp = Graph.getExit(3);
            if (tmp)
                tmp.isExit = true;
            spawnValues = new Vector3(57f, 0.9f, -45f);
        }
        else
        {
            /*Node tmp = Graph.getExit(4);
            if (tmp)
                tmp.isExit = true;*/
            spawnValues = new Vector3(-50f, 0.9f, -43f);
        }

        Quaternion spawnRotation = Quaternion.identity;
        Instantiate(playerObject, spawnValues, spawnRotation);
        for (int i = 0; i < nbrAllies; i++)
        {
            Vector3 spawnPosition = new Vector3(spawnValues.x, spawnValues.y, spawnValues.z);
            spawnPosition = new Vector3(Random.Range(spawnValues.x - 1, spawnValues.x + 1), spawnValues.y, Random.Range(spawnValues.z - 1, spawnValues.z + 1));
            spawnRotation = Quaternion.identity;
            Instantiate(ally, spawnPosition, spawnRotation);
        }
    }

	void Update ()
    {
        for (int i = 0; i < Graph.Nodes.Count; i++)
        {
            if (Graph.Nodes[i].nearbyEnemiesValue > 0)
                Graph.Nodes[i].nearbyEnemiesValue -= 5;
            if (Graph.Nodes[i].nearbyEnemiesValue < 0)
                Graph.Nodes[i].nearbyEnemiesValue = 0;
            Graph.Nodes[i].nearbyEnemiesNumber = 0;
        }

        List<Enemy> allEnemies = new List<Enemy>();
        /*for (int i = 0; i < EnemyDirector.allEnemies.Count; i++)
        {
            EnemyDirector.allEnemies[i].renderer.enabled = false;
            EnemyDirector.allEnemies[i].GetComponent<Enemy>().MyHurtBar.renderer.enabled = false;
            EnemyDirector.allEnemies[i].GetComponent<Enemy>().MyRechargeBar.renderer.enabled = false;
            EnemyDirector.allEnemies[i].GetComponent<Enemy>().MyLifeBar.renderer.enabled = false;
        }*/
        for (int i = 0; i < allAllies.Count; i++)
        {
            for (int j = 0; j < allAllies[i].visibleEnemies.Count; j++)
            {
                if (allAllies[i].visibleEnemies[j] && !allEnemies.Contains(allAllies[i].visibleEnemies[j]))
                {
                    allEnemies.Add(allAllies[i].visibleEnemies[j]);
                    allAllies[i].visibleEnemies[j].currentNode.nearbyEnemiesNumber++;
                    allAllies[i].visibleEnemies[j].renderer.enabled = true;
                    allAllies[i].visibleEnemies[j].GetComponent<Enemy>().MyHurtBar.renderer.enabled = true;
                    allAllies[i].visibleEnemies[j].GetComponent<Enemy>().MyRechargeBar.renderer.enabled = true;
                    allAllies[i].visibleEnemies[j].GetComponent<Enemy>().MyLifeBar.renderer.enabled = true;
                    allAllies[i].visibleEnemies[j].GetComponent<Enemy>().seenTime = Time.time;
                    allAllies[i].visibleEnemies[j].light.enabled = true;
                    allAllies[i].visibleEnemies[j].GetComponent<Enemy>().t = 0;
                }
            }
        }
        for (int j = 0; j < player.visibleEnemies.Count; j++)
        {
            if (player.visibleEnemies[j] && !allEnemies.Contains(player.visibleEnemies[j]))
            {
                allEnemies.Add(player.visibleEnemies[j]);
                player.visibleEnemies[j].currentNode.nearbyEnemiesNumber++;
                player.visibleEnemies[j].renderer.enabled = true;
                player.visibleEnemies[j].GetComponent<Enemy>().MyHurtBar.renderer.enabled = true;
                player.visibleEnemies[j].GetComponent<Enemy>().MyRechargeBar.renderer.enabled = true;
                player.visibleEnemies[j].GetComponent<Enemy>().MyLifeBar.renderer.enabled = true;
                player.visibleEnemies[j].GetComponent<Enemy>().seenTime = Time.time;
                player.visibleEnemies[j].light.enabled = true;
                player.visibleEnemies[j].GetComponent<Enemy>().t = 0;
            }
        }

        for (int i = 0; i < Graph.Nodes.Count; i++)
        {
            if (Graph.Nodes[i].nearbyEnemiesNumber > 0)
                Graph.Nodes[i].assignEnemyValue(8 + Graph.Nodes[i].nearbyEnemiesNumber);
        }

        Graph.computeDistancesToExit();
        Graph.drawRecommendedPath(player.currentNode);
        playerNode = player.currentNode;

        if (playerNode)
        {
            List<Node> sortedlist = new List<Node>(playerNode.TargetNodes);
            sortedlist.Sort(compareExitDistances);
            int numberEnemies = 0;

            for (int i = 0; i < sortedlist.Count; i++)
            {
                numberEnemies += sortedlist[i].nearbyEnemiesValue;
            }
            if (sortedlist[0].nearbyEnemiesValue > 0)
            {
                for (int i = 0; i < allAllies.Count; i++)
                {
                    allAllies[i].targetNode = sortedlist[0];
                    if (allAllies[i].state() != "Attack")
                        allAllies[i].currentColor = Ally.attackColor;
                    allAllies[i].agent.updateRotation = true;
                }
            }
            else
            {
                int iter = 0;

                for (int i = 0; i < allAllies.Count; i++)
                {
                    allAllies[i].targetNode = sortedlist[iter];
                    allAllies[i].currentColor = Ally.coverColor;
                    iter++;
                    if (iter >= sortedlist.Count)
                        iter = 0;
                }
            }
        }

        // Scout is disabled
        /*if (allAllies.Count > 1)
        {
            allAllies[0].scout = true;
            allAllies[0].agent.updateRotation = true;
        }*/
	}

    public int compareExitDistances(Node n1, Node n2)
    {
        if (n1.nearbyEnemiesValue < n2.nearbyEnemiesValue)
            return 1;
        if (n1.nearbyEnemiesValue > n2.nearbyEnemiesValue)
            return -1;

        if (n1.distanceToExit > n2.distanceToExit)
            return 1;
        if (n1.distanceToExit < n2.distanceToExit)
            return -1;
        return 0;
    }
}
