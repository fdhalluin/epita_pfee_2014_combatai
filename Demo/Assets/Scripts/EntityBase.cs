using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

// Display bars
public abstract class EntityBase : StateBehaviour
{
	protected abstract float HpRatio { get; }
    protected abstract float ShieldRatio { get; }
	protected abstract float RechargeRatio { get; }

	public GameObject lifeBar;
	public GameObject hurtBar;
	public GameObject rechargeBar;
    public GameObject shieldBar;
	public GameObject MyLifeBar;
	public GameObject MyHurtBar;
	public GameObject MyRechargeBar;
    public GameObject MyShieldBar;

	public void InitBars()
	{
		Vector3 spawnPosition = Vector3.zero;
		Quaternion spawnRotation = Quaternion.identity;
		MyLifeBar = (GameObject)Instantiate(lifeBar, spawnPosition, spawnRotation);
		MyLifeBar.transform.parent = transform;
		MyHurtBar = (GameObject)Instantiate(hurtBar, spawnPosition, spawnRotation);
		MyHurtBar.transform.parent = transform;
		MyRechargeBar = (GameObject)Instantiate(rechargeBar, spawnPosition, spawnRotation);
		MyRechargeBar.transform.parent = transform;

        MyShieldBar = (GameObject)Instantiate(shieldBar, spawnPosition, spawnRotation);
        MyShieldBar.transform.parent = transform;

		UpdateBars();
	}

	public void UpdateBars()
	{
		// Orient bars to camera orientation
		var cameraForward = Camera.main.transform.forward;
		cameraForward.y = 0;
		cameraForward.Normalize();
		var cameraRotation = Quaternion.LookRotation(cameraForward);
		
		float barWidth = 2.5f;
		float barHeight = 0.2f;
		float barDepth = 0.2f;
		
		float lifeHeight = 1.2f;
		float rechargeHeight = 1.2f;
        float shieldHeight = 1.2f;

		Vector3 cameraOffsetLife = Camera.main.transform.up * 0.7f; // shift slightly in camera space (ideally, should shift in screen space)
        Vector3 cameraOffsetShield = Camera.main.transform.up * 0.9f; 
		Vector3 cameraOffsetCooldown = Camera.main.transform.up * 0.5f;
		
		float scaleX = (transform.localScale.x + transform.localScale.z) * 0.5f;
		float scaleY = transform.localScale.y;
		
		float lifeProgress = Mathf.Clamp01(HpRatio);
		MyLifeBar.transform.rotation = cameraRotation;
		MyLifeBar.transform.position = transform.position + cameraRotation * new Vector3(barWidth * (lifeProgress - 1) * 0.5f, lifeHeight, 0) + cameraOffsetLife;
		MyLifeBar.transform.localScale = new Vector3(barWidth * lifeProgress / scaleX, barHeight / scaleY, barDepth / scaleX);

        float shieldProgress = Mathf.Clamp01(ShieldRatio);
        MyShieldBar.transform.rotation = cameraRotation;
        MyShieldBar.transform.position = transform.position + cameraRotation * new Vector3(barWidth * (shieldProgress - 1) * 0.5f, shieldHeight, 0) + cameraOffsetShield;
        MyShieldBar.transform.localScale = new Vector3(barWidth * shieldProgress / scaleX, barHeight / scaleY, barDepth / scaleX);

		MyHurtBar.transform.rotation = cameraRotation;
		MyHurtBar.transform.position = transform.position + cameraRotation * new Vector3(barWidth * (lifeProgress) * 0.5f, lifeHeight, 0) + cameraOffsetLife;
		MyHurtBar.transform.localScale = new Vector3(barWidth * (1 - lifeProgress) / scaleX, barHeight / scaleY, barDepth / scaleX);
		
		float rechargeProgress = Mathf.Clamp01(RechargeRatio);
		MyRechargeBar.transform.rotation = cameraRotation;
		MyRechargeBar.transform.position = transform.position + cameraRotation * new Vector3(barWidth * (rechargeProgress - 1) * 0.5f, rechargeHeight, 0) + cameraOffsetCooldown;
		MyRechargeBar.transform.localScale = new Vector3(barWidth * rechargeProgress / scaleX, barHeight / scaleY, barDepth / scaleX);

		MyLifeBar.renderer.enabled = HpRatio > 0;
        MyShieldBar.renderer.enabled = ShieldRatio > 0;
		MyHurtBar.renderer.enabled = HpRatio < 1;
		MyRechargeBar.renderer.enabled = RechargeRatio > 0;
	}
}