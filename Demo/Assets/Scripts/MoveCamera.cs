﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour {

    public float cameraZoomDamping = 0.2f;
    public float cameraZoomSpeed = 1.0f;
    public float cameraSpeed = 5f;
    public float cameraMinFov = 25f;
    public float cameraMaxFov = 90f;
 
    private float zoom = 0.5f;
    private float targetZoom = 0.5f;
 
    void Update() {
        // Move
        Vector3 move = new Vector3();

        if (Input.GetKey(KeyCode.UpArrow))
            move.z += cameraSpeed;
        if (Input.GetKey(KeyCode.DownArrow))
            move.z -= cameraSpeed;
        if (Input.GetKey(KeyCode.LeftArrow))
            move.x -= cameraSpeed;
        if (Input.GetKey(KeyCode.RightArrow))
            move.x += cameraSpeed;

        transform.position += move;

        // Zoom
        targetZoom -= Input.GetAxis("Mouse ScrollWheel") * cameraZoomSpeed;
        targetZoom = Mathf.Clamp01(targetZoom);

        // Damping
        if (Time.deltaTime > 0)
            zoom = Mathf.Lerp(zoom, targetZoom, Time.deltaTime / (Time.deltaTime + cameraZoomDamping));

        camera.fieldOfView = Mathf.Lerp(cameraMinFov, cameraMaxFov, zoom);
    }
}
