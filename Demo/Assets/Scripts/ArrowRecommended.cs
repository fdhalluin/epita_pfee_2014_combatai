﻿using UnityEngine;
using System.Collections;

public class ArrowRecommended : MonoBehaviour {

    private float speed = 6.0f;
    public static Node recommended = null;
    public static Node oldRecommended = null;

    void Start()
    {
        renderer.enabled = false;
    }

    void Update()
    {
        if (!renderer.enabled && GameController.getDrawArrowRecommended())
            renderer.enabled = true;

        if (recommended != oldRecommended && recommended != null)
        {
            transform.position = recommended.transform.position;
            renderer.enabled = true;
            oldRecommended = recommended;
        }

        if (recommended != null)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + speed / 10, transform.position.z);
            speed -= 0.2f;
            if (transform.position.y <= 3)
                speed = 5;
        }
        else
        {
            renderer.enabled = false;
            oldRecommended = null;
        }

        if (renderer.enabled && !GameController.getDrawArrowRecommended())
            renderer.enabled = false;
    }
}
