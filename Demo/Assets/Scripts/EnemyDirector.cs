﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyDirector : MonoBehaviour {

    public static List<Enemy> allEnemies = new List<Enemy>();
    public static List<int> assignement = new List<int>();

    public static Node knownPlayerPosition = null;
    public static Node oldKnownPlayerPosition = null;
    public int divideByGroups = 4;

    public GameObject ennemy;
    public Vector3 spawnValues;
    public static float maxHp = 50;
    public static int attack = 10;
    public static int nbrEnemies = 15;
    public static int aggressivity = 50;

    void Start ()
    {
        SpawnEnnemies();
	}

    void SpawnEnnemies()
    {
        for (int i = 0; i < nbrEnemies; i++)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(spawnValues.x - 4, spawnValues.x + 4), spawnValues.y, Random.Range(spawnValues.z - 5, spawnValues.z + 5));
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(ennemy, spawnPosition, spawnRotation);
        }
    }
    
	void Update ()
    {
        if (knownPlayerPosition && !knownPlayerPosition.Equals(oldKnownPlayerPosition))
        {
            for (int i = 0; i < allEnemies.Count; i++)
            {
                allEnemies[i].assigned = false;
            }

            oldKnownPlayerPosition = knownPlayerPosition;

            List<Node> flankPoints = PathCalculator.getFlankPoints(knownPlayerPosition);
            int index_flank = 0;
            int count = 0;

            for (int index_ennemy = 0; index_ennemy < ((float) EnemyDirector.aggressivity / (float) 100) * allEnemies.Count; index_ennemy++)
            {
                allEnemies[index_ennemy].targetNode = flankPoints[index_flank];
                allEnemies[index_ennemy].assigned = true;
                allEnemies[index_ennemy].currentColor = Enemy.attackColor;
                allEnemies[index_ennemy].timeLeftAssigned = 5.0f;

                index_flank++;
                if (index_flank >= flankPoints.Count)
                    index_flank = 0;
                count++;
            }

            int index_ambush = 0;
            count = 0;

            for (int index_ennemy = (int)(((float)EnemyDirector.aggressivity / (float)100) * allEnemies.Count); index_ennemy < allEnemies.Count; index_ennemy++)
            {
                allEnemies[index_ennemy].targetNode = PathCalculator.ambushPoints[index_ambush];
                allEnemies[index_ennemy].assigned = true;
                allEnemies[index_ennemy].currentColor = Enemy.ambushColor;
                allEnemies[index_ennemy].timeLeftAssigned = 5.0f;

                index_ambush++;
                if (index_ambush >= PathCalculator.ambushPoints.Count)
                    index_ambush = 0;
                count++;
            }
        }
	}
}
