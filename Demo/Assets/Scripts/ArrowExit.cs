﻿using UnityEngine;
using System.Collections;

public class ArrowExit : MonoBehaviour {

    private bool started = false;
    private float speed = 6.0f;

	void Start()
    {
        started = false;
        renderer.enabled = false;
	}
	
	void Update()
    {
        if (!renderer.enabled && GameController.getDrawArrowExit())
            renderer.enabled = true;

        if (!started)
        {
            for (int i = 0; i < Graph.Nodes.Count; i++)
            {
                if (Graph.Nodes[i].isExit)
                {
                    transform.position = Graph.Nodes[i].transform.position;
                    break;
                }
            }
            started = true;
        }

        transform.position = new Vector3(transform.position.x, transform.position.y + speed / 10, transform.position.z);
        speed -= 0.2f;

        if (transform.position.y <= 3)
            speed = 5;

        if (renderer.enabled && !GameController.getDrawArrowExit())
            renderer.enabled = false;
	}
}
