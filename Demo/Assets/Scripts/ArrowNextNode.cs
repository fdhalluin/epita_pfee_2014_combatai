﻿using UnityEngine;
using System.Collections;

public class ArrowNextNode : MonoBehaviour {

    private float speed = 6.0f;
    public static Node oldNextNode = null;
    public static Node nextNode = null;

    void Start()
    {
        renderer.enabled = false;
    }

    void Update()
    {
        if (!renderer.enabled && GameController.getDrawArrowNextNode())
            renderer.enabled = true;

        if (nextNode != oldNextNode && nextNode != null)
        {
            transform.position = nextNode.transform.position;
            renderer.enabled = true;
            oldNextNode = nextNode;
        }

        if (nextNode != null)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + speed / 10, transform.position.z);
            speed -= 0.2f;
            if (transform.position.y <= 3)
                speed = 5;
        }
        else
        {
            renderer.enabled = false;
            oldNextNode = null;
        }

        if (renderer.enabled && !GameController.getDrawArrowNextNode())
            renderer.enabled = false;
    }
}
