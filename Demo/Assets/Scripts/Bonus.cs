﻿using UnityEngine;
using System.Collections;

public abstract class Bonus : MonoBehaviour
{
    protected bool stopBonus = true;
    protected bool taken = false;
    protected PlayerMovement player; 
    public int duration = 10;
    public int rotationSpeed = 30;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public virtual void Update()
    {
        transform.Rotate(rotationSpeed * Vector3.up * Time.deltaTime, Space.World);

        if (Menu.bonusesS && !taken)
            Show();
        else
            Hide();
    }

    public virtual void Hide()
    {
        renderer.enabled = false;
        light.enabled = false;
    }
    public virtual void Show()
    {
        renderer.enabled = true;
        light.enabled = true;
    } 

    void OnTriggerEnter(Collider c)
    {
        if (!renderer.enabled)
            return;

        if (c.CompareTag("Player"))
        {
            taken = true;
            player = c.GetComponent<PlayerMovement>();
            StartCoroutine("StartBonus");
        }
    }

    private IEnumerator StartBonus()
    {
        BonusActivate();
        yield return new WaitForSeconds(duration);
        //if (stopBonus)
        BonusDesactivate();
    }

    public abstract void BonusActivate();
    public abstract void BonusDesactivate();
}
