﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]

public class Graph : MonoBehaviour {

    public static List<Node> Nodes = new List<Node>();
    private bool computed = false;

	void Start () {
	}
	
	void Update () {
        if (!computed)
        {
            computeDistances();
            computed = true;
        }
	}

    public static Node getNode(Transform transform)
    {
        for (int i = 0; i < Nodes.Count; i++)
        {
            Node n = Nodes[i];
            if (n.transform.Equals(transform)) return n;
        }
        return null;
    }

    public static Node getNode(int id)
    {
        for (int i = 0; i < Nodes.Count; i++)
        {
            Node n = Nodes[i];
            if (n.id == id) return n;
        }
        return null;
    }

    public static Node getExit(int exit)
    {
        for (int i = 0; i < Nodes.Count; i++)
        {
            Node n = Nodes[i];
            if (n.possibleExit == exit) return n;
        }
        return null;
    }

    public static Node getNearestNode(float x, float z)
    {
        float distMax = float.MaxValue;
        Node best = null;

        for (int i = 0; i < Nodes.Count; i++)
        {
            Node n = Nodes[i];
            if (n != null)
            {
                float X = x - n.transform.position.x;
                float Z = z - n.transform.position.z;

                float distance = X * X + Z * Z;
                if (distance < distMax)
                {
                    distMax = distance;
                    best = n;
                }
            }
        }
        return best;
    }

    public static Node getNearestNode(Transform transform)
    {
        float distMax = float.MaxValue;
        Node best = null;

        for (int i = 0; i < Nodes.Count; i++)
        {
            Node n = Nodes[i];
            if (transform != null && n != null)
            {
                float X = transform.position.x - n.transform.position.x;
                float Y = transform.position.y - n.transform.position.y;
                float Z = transform.position.z - n.transform.position.z;

                float distance = X * X + Y * Y + Z * Z;
                if (distance < distMax)
                {
                    distMax = distance;
                    best = n;
                }
            }
        }
        return best;
    }

    public static List<Node> getShortestPath(Node start, Node end)
    {
        List<Node> result = new List<Node>();

        result.Add(start);

        while (start != end)
        {
            float min = start.PlayerMovementInterest;
            Node best = null;
            for (int i = 0; i < start.TargetNodes.Count; i++)
            {
                if (start.TargetNodes[i].PlayerMovementInterest < min)
                {
                    min = start.TargetNodes[i].PlayerMovementInterest;
                    best = start.TargetNodes[i];
                }
            }
            result.Add(best);
            start = best;
        }

        return result;
    }

    public static void computeDistances()
    {
        for (int i = 0; i < Nodes.Count; i++)
        {
            Node n = Nodes[i];
            n.distanceToExit = 0.0f;
            if (n.TargetDistances != null)
                n.TargetDistances.Clear();
            else
                n.TargetDistances = new List<float>();
            for (int j = 0; j < n.TargetNodes.Count; j++)
                n.TargetDistances.Add(0.0f);

            if (n.ParentDistances != null)
                n.ParentDistances.Clear();
            else
                n.ParentDistances = new List<float>();
            for (int j = 0; j < n.ParentNodes.Count; j++)
                n.ParentDistances.Add(0.0f);
        }

        for (int i = 0; i < Nodes.Count; i++)
        {
            Node n = Nodes[i];
            for (int iter = 0; iter < n.TargetNodes.Count; iter++)
            {
                n.TargetDistances[iter] = n.distanceTo(n.TargetNodes[iter]);
            }

            for (int iter = 0; iter < n.ParentNodes.Count; iter++)
            {
                n.ParentDistances[iter] = n.distanceTo(n.ParentNodes[iter]);
            }
        }
        computeDistancesToExit();
    }

    public static void computeDistancesToExit()
    {
        for (int i = 0; i < Nodes.Count; i++)
        {
            Nodes[i].distanceToExit = 0.0f;
        }

        for (int i = 0; i < Graph.Nodes.Count; i++)
        {
            for (int j = 0; j < Graph.Nodes.Count; j++)
            {
                Node n = Nodes[j];
                float min = 0.0f;
                int best = -1;
                for (int iter = 0; iter < n.TargetNodes.Count; iter++)
                {
                    Node target = n.TargetNodes[iter];
                    if (target.isExit)
                    {
                        n.distanceToExit = n.TargetDistances[iter] + AllyDirector.riskCoef * 15 * (n.nearbyEnemies.Count + target.nearbyEnemies.Count) / 2;
                        break;
                    }

                    if (target.distanceToExit > 0.0001f &&
                        (min < 0.0001f || target.distanceToExit < min))
                    {
                        min = target.distanceToExit;
                        best = iter;
                    }
                }
                if (best != -1 && (n.distanceToExit < 0.0001f || n.distanceToExit > min + n.TargetDistances[best] + 0.0001f) && !n.isExit)
                    n.distanceToExit = min + n.TargetDistances[best] + AllyDirector.riskCoef * 15 * (n.nearbyEnemies.Count + n.TargetNodes[best].nearbyEnemies.Count) / 2;
            }
        }
    }

    public static void drawRecommendedPath(Node playerNode)
    {
        bool setRecommended = true;

        for (int i = 0; i < Graph.Nodes.Count; i++)
        {
            Graph.Nodes[i].highlightRecommendedPath = false;
            Graph.Nodes[i].MylinkBestPath.renderer.enabled = false;
            Graph.Nodes[i].linkDrawn = false;
        }

        if (playerNode)
        {
            int iter = 0;
            playerNode.highlightRecommendedPath = true;
            while (!playerNode.isExit && iter < 100)
            {
                float min = float.MaxValue;
                Node best = null;
                for (int i = 0; i < playerNode.TargetNodes.Count; i++)
                {
                    if (playerNode.TargetNodes[i].distanceToExit < min)
                    {
                        min = playerNode.TargetNodes[i].distanceToExit;
                        best = playerNode.TargetNodes[i];
                    }
                }
                if (GameController.getDrawRecommendedPath())
                    playerNode.drawLink(best);
                playerNode = best;
                if (setRecommended)
                {
                    ArrowRecommended.recommended = playerNode;
                }
                setRecommended = false;
                playerNode.highlightRecommendedPath = true;
                iter++;
            }
        }
    }
}
