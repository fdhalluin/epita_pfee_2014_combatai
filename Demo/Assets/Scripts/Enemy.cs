﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class Enemy : EntityBase
{
    public enum AttackState
    {
        Ready,
        Shooting,
        Reloading
    };

	//public EnemyState state;
    public AttackState attackState;

	protected override float HpRatio
	{
		get { return hp / EnemyDirector.maxHp; }
	}

    protected override float ShieldRatio
    {
        get { return 0; }
    }

	protected override float RechargeRatio
	{
		get { return 1 - Mathf.Clamp01(recharging / MaxTimeRecharging); }
	}

    public float seenTime = float.MinValue;
    public float disapearTime;

    public bool assigned = false;
    private NavMeshAgent agent;
    private float shooting;
    public Node currentNode;
    public Node targetNode;
    private float timeLeft = 1.0f;
    public float timeLeftAssigned = 1.0f;

    private List<Ally> nearbyEntites;
    private Ally targetEntity;
    public Ally attackingAlly;
    public float attackingAllyCountdown;

    private PlayerMovement nearbyPlayer;
    private PlayerMovement targetPlayer;

	protected float hp = EnemyDirector.maxHp;
	protected float MaxTimeRecharging = 2.0f;
	protected float recharging;

	public GameObject bullet;
	protected Bullet MyBullet;

    private Vector3 origin;

    public Color initialOutlineColor;
    public Color currentColor;
    public static Color searchColor = Color.yellow;
    public static Color attackColor = new Color(55.0f / 255.0f, 33.0f / 255.0f, 0); // dark orange
    public static Color fleeColor = new Color(254.0f / 255.0f, 254.0f / 255.0f, 125.0f / 255.0f); // light yellow
    public static Color ambushColor = new Color(255.0f / 255.0f, 172.0f / 255.0f, 46.0f / 255.0f); // light orange
    public static Color shootColor = Color.red;

    public float initialLightIntensity;
    public GameObject death;
    private float recovery;
    private bool flee;
    public static float enemySpeed = 3.0f;

    void Start()
    {
        if (!EnemyDirector.allEnemies.Contains(this))
            EnemyDirector.allEnemies.Add(this);
        agent = GetComponent<NavMeshAgent>();
        agent.speed = enemySpeed;
        targetNode = null;
        currentNode = null;
        targetEntity = null;
        targetPlayer = null;
        timeLeft = 1.0f;
        timeLeftAssigned = 1.0f;
        recharging = 0.0f;
        shooting = 0.0f;

        setState("Search");

        blackboard.GetFloatVar("FloatHp").Value = hp;

        attackState = AttackState.Ready;
        nearbyEntites = new List<Ally>();
        nearbyPlayer = null;
        origin = transform.position;

        attackingAlly = null;
        attackingAllyCountdown = 0;

        renderer.enabled = false;
        light.enabled = false;

        initialLightIntensity = light.intensity;
        initialOutlineColor = renderer.material.GetColor("_OutlineColor");
        currentColor = searchColor;
        seenTime = float.MinValue;
        t = 0;
        recovery = 0;
        flee = false;

		InitBars();
	}

    public float t;
    void OnDestroy()
    {
        if (EnemyDirector.allEnemies.Contains(this))
            EnemyDirector.allEnemies.Remove(this);
    }

    void Update()
    {
        recovery -= Time.deltaTime;
        if (recovery <= 0 && hp < EnemyDirector.maxHp)
        {
			hp = Mathf.Min(hp + GameController.recover_speed * EnemyDirector.maxHp * Time.deltaTime, EnemyDirector.maxHp);
        }

        nearbyEntites.Clear();
        nearbyPlayer = null;
        targetPlayer = null;

        var hitColliders = Physics.OverlapSphere(transform.position, PlayerMovement.maxRadius);

        for (var i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].tag == "Ally")
                nearbyEntites.Add(hitColliders[i].gameObject.GetComponent<Ally>());

            if (hitColliders[i].tag == "Player")
                nearbyPlayer = hitColliders[i].gameObject.GetComponent<PlayerMovement>();
        }

        currentNode = Graph.getNearestNode(transform);
        if (currentNode != null)
            currentNode.visitEnemy();

        blackboard.GetFloatVar("FloatHp").Value = hp;
		if (hp <= 0)
		{
			var deathEffect = Instantiate(death, transform.position, transform.rotation) as GameObject;
			var deathParticle = deathEffect.GetComponent<ParticleSystem>();
			if (deathParticle != null)
			{
				deathParticle.startColor = currentColor + Color.white * 0.15f; // Let additive particles merge to white
			}
		}

        if (recharging > 0.0f)
            recharging -= Time.deltaTime;
        if (shooting > 0.0f)
            shooting -= Time.deltaTime;
        if (attackingAllyCountdown > 0.0f && targetEntity == null)
        {
            attackingAllyCountdown -= Time.deltaTime;
        }
        else if (attackingAlly != null)
        {
            attackingAlly = null;
        }

        if (hp <= 15)
        {
            flee = true;
            setState("Flee");
        }
        else if (flee)
        {
            flee = false;
            currentColor = searchColor;
            setState("Search");
        }


        switch (state())
        {
            case "Search":
                updateSearch();
                break;

            case "Attack":
                currentColor = shootColor;
                updateAttack();
                break;

            case "Flee":
                currentColor = fleeColor;
                updateFlee();
                break;
        }

		// Protip: Do the visual update after all logic update, otherwise visual will lag one frame behind
		UpdateBars();
		
		//renderer.material.SetColor("_OutlineColor", new Color(renderer.material.GetColor("_OutlineColor").r, renderer.material.GetColor("_OutlineColor").g, renderer.material.GetColor("_OutlineColor").b));
		if (Menu.fowS)
		{
			if (t < 1)
				t += Time.deltaTime / 3;
			
			renderer.material.SetColor("_OutlineColor", Color.Lerp(currentColor, Color.black, t));
			
			light.intensity *= 0.9f;
			
			if (Time.time > seenTime + 3)
			{
				renderer.enabled = false;
				MyHurtBar.renderer.enabled = false;
				MyLifeBar.renderer.enabled = false;
				MyRechargeBar.renderer.enabled = false;
				light.enabled = false;
			}
			else
			{
				renderer.material.SetColor("_Color", currentColor);
				renderer.material.SetColor("_OutlineColor", currentColor);
				light.intensity = initialLightIntensity;
			}
		}
		else
		{
			renderer.enabled = true;
			MyHurtBar.renderer.enabled = true;
			MyLifeBar.renderer.enabled = true;
			MyRechargeBar.renderer.enabled = true;
			light.enabled = true;
			light.intensity = initialLightIntensity;
			renderer.material.SetColor("_Color", currentColor);
			renderer.material.SetColor("_OutlineColor", currentColor);
		}
    }

    public void testAttack()
    {
        //Debug.Log("test attack");
        bool foundEntity = false;
        bool foundPlayer = false;

        for (int i = 0; i < nearbyEntites.Count; i++)
        {
            Vector3 sight = new Vector3(nearbyEntites[i].transform.position.x - transform.position.x, nearbyEntites[i].transform.position.y - transform.position.y, nearbyEntites[i].transform.position.z - transform.position.z);
            float a = Vector3.Angle(transform.forward, sight);
            if (Mathf.Abs(a) < PlayerMovement.angleView / 2)
            {
                RaycastHit hit;

                int layerMask = 1 << LayerMask.NameToLayer("Obstacle");
                if (!Physics.Raycast(transform.position, (nearbyEntites[i].transform.position - transform.position), out hit, distance(nearbyEntites[i].transform.position, transform.position), layerMask))
                {
                    //state = EnemyState.Attack;
                    targetEntity = nearbyEntites[i];

                    EnemyDirector.knownPlayerPosition = targetEntity.currentNode;

                    blackboard.GetGameObjectVar("TargetEntity").Value = targetEntity.gameObject;

                    foundEntity = true;
                    break;
                }
            }
        }
        if (!foundEntity)
        {
            blackboard.GetGameObjectVar("TargetEntity").Value = null;
            targetEntity = null;
        }

        if (nearbyPlayer != null && nearbyPlayer.isInvisible == false)
        {
            Vector3 sight = new Vector3(nearbyPlayer.transform.position.x - transform.position.x, nearbyPlayer.transform.position.y - transform.position.y, nearbyPlayer.transform.position.z - transform.position.z);
            float a = Vector3.Angle(transform.forward, sight);
            if (Mathf.Abs(a) < PlayerMovement.angleView / 2)
            {
                RaycastHit hit;

                int layerMask = 1 << LayerMask.NameToLayer("Obstacle");
                if (!Physics.Raycast(transform.position, (nearbyPlayer.transform.position - transform.position), out hit, distance(nearbyPlayer.transform.position, transform.position), layerMask))
                {
                    //state = EnemyState.Attack;

                    targetPlayer = nearbyPlayer;

                    blackboard.GetGameObjectVar("TargetPlayer").Value = targetPlayer.gameObject;

                    foundPlayer = true;

                    EnemyDirector.knownPlayerPosition = nearbyPlayer.currentNode;
                    //return;
                }
            }
        }

        if (!foundPlayer)
        {
            blackboard.GetGameObjectVar("TargetPlayer").Value = null;
            targetPlayer = null;
        }
    }

    public static float distance(Vector3 t1, Vector3 t2)
    {
        float X = t1.x - t2.x;
        float Y = t1.y - t2.y;
        float Z = t1.z - t2.z;

        return Mathf.Sqrt(X * X + Y * Y + Z * Z);
    }

    public void updateSearch()
    {
        float distance = 20;
        targetEntity = null;

        if (targetNode != null)
        {
            float X = targetNode.transform.position.x - transform.position.x;
            float Y = targetNode.transform.position.y - transform.position.y;
            float Z = targetNode.transform.position.z - transform.position.z;

            distance = X * X + Y * Y + Z * Z;
        }

        if (!assigned)
        {
            if (timeLeft <= 0.0f && (targetNode == null || distance < 5))
            {
                targetNode = chooseLeastVisited(currentNode.TargetNodes);
                timeLeft = 1.0f;
            }
            else
                timeLeft -= Time.deltaTime;
        }
        else
        {
            if (distance < 5)
                timeLeftAssigned -= Time.deltaTime;
            if (timeLeftAssigned <= 0.0f)
            {
                assigned = false;
                currentColor = searchColor;
            }
        }

        if (targetNode != null)
            agent.SetDestination(targetNode.transform.position);
        else if (currentNode != null)
            agent.SetDestination(currentNode.transform.position);

        if (targetEntity == null && attackingAlly != null)
        {
            Ally.rotateToward(transform, attackingAlly.transform, transform.forward, transform.up);
        }

        testAttack();
    }

    public void updateAttack()
    {
        agent.SetDestination(transform.position);

        switch (attackState)
        {
            case AttackState.Ready:
                updateReady();
                break;

            case AttackState.Reloading:
                updateReloading();
                break;

            case AttackState.Shooting:
                updateShooting();
                break;
        }
    }

    public void updateReady()
    {
        testAttack();
        if (state() == "Attack")
        {
            if (targetPlayer != null)
            {
                if (recharging <= 0.0f)
                {
                    targetPlayer.hit(EnemyDirector.attack);
                    shooting = 0.2f;
                    recharging = MaxTimeRecharging + shooting;
                    attackState = AttackState.Shooting;

                    GameObject MyBulletObject = (GameObject)Instantiate(bullet, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
                    Bullet MyBullet = MyBulletObject.GetComponent<Bullet>();

                    MyBullet.transform.Rotate(new Vector3(0, 0, 1), 90.0f);

                    Ally.rotateToward(MyBullet.transform, targetPlayer.transform, MyBullet.transform.up, new Vector3(0, 1, 0));

                    MyBullet.objectToDestroy = MyBulletObject;
                    MyBullet.targetEnemy = null;
                    MyBullet.targetAlly = null;
                    MyBullet.targetPlayer = targetPlayer;
                    MyBullet.startPos = transform.position;

                    Ally.rotateToward(transform, targetPlayer.transform, transform.forward, transform.up);
                }
            }
            else if (targetEntity != null)
            {
                if (recharging <= 0.0f)
                {
                    targetEntity.hit(EnemyDirector.attack);

                    targetEntity.attackingEnemy = this;
                    targetEntity.attackingEnemyCountdown = 2;

                    shooting = 0.2f;
                    recharging = MaxTimeRecharging + shooting;
                    attackState = AttackState.Shooting;

                    GameObject MyBulletObject = (GameObject)Instantiate(bullet, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
                    Bullet MyBullet = MyBulletObject.GetComponent<Bullet>();

                    MyBullet.transform.Rotate(new Vector3(0, 0, 1), 90.0f);

                    Ally.rotateToward(MyBullet.transform, targetEntity.transform, MyBullet.transform.up, new Vector3(0, 1, 0));

                    MyBullet.objectToDestroy = MyBulletObject;
                    MyBullet.targetEnemy = null;
                    MyBullet.targetAlly = targetEntity;
                    MyBullet.targetPlayer = null;
                    MyBullet.startPos = transform.position;

                    Ally.rotateToward(transform, targetEntity.transform, transform.forward, transform.up);
                }
            }
        }
    }

    public void updateReloading()
    {
        if (recharging <= 0)
        {
            attackState = AttackState.Ready;
        }
    }

    public void updateShooting()
    {
        if (shooting <= 0)
        {
            attackState = AttackState.Reloading;
            recharging = MaxTimeRecharging;
        }
    }

    public void updateBypass()
    {
    }

    public void updateFlee()
    {
        int min = int.MaxValue;
        Node best = null;
        for (int i = 0; i < currentNode.TargetNodes.Count; i++)
        {
            int allies_count = currentNode.TargetNodes[i].getAdjacentAlliesCount();
            if (allies_count < min)
            {
                min = allies_count;
                best = currentNode.TargetNodes[i];
            }
        }
        if (best != null)
            agent.SetDestination(best.transform.position);
    }

    public void updateWait()
    {
        agent.SetDestination(transform.position);
    }

    public void updateDead()
    {
    }

    public float getHp()
    {
        return hp;
    }

    public void hit(int attack)
    {
        recovery = GameController.recover_time; ;
        hp -= attack;
        if (hp <= 0)
            hp = 0;
    }

    private Node chooseLeastVisited(List<Node> targets)
    {
        bool finished = false;
        List<Node> candidates = new List<Node>();
        float min = float.MaxValue;

        while (!finished)
        {
            finished = true;
            candidates.Clear();
            for (int iter = 0; iter < targets.Count; iter++)
            {
                Node n = targets[iter];
                if (n.getLastVisitedEnemy() < min)
                {
                    min = n.getLastVisitedEnemy();
                    finished = false;
                    break;
                }

                if (n.getLastVisitedEnemy() == min)
                {
                    candidates.Add(n);
                }
            }
        }

        int count = candidates.Count;
        return candidates[Random.Range(0, count)];
    }

    void setState(string state)
    {
        blackboard.GetStringVar("State").Value = state;
    }

    string state()
    {
        return blackboard.GetStringVar("State").Value;
    }
}
