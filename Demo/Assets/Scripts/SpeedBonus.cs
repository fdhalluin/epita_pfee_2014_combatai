﻿using UnityEngine;
using System.Collections;

public class SpeedBonus : Bonus {

    public int speedMultiplicator = 2;
    private float t = 0;
    private Color initColor;
	// Use this for initialization
	void Start () {
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
    }

    public override void BonusActivate()
    {
        initColor = player.renderer.material.color;
        player.agent.speed *= speedMultiplicator;
        StartCoroutine("UpdateColor");
    }

    private IEnumerator UpdateColor()
    {
        while (true)
        {
            if (t < 1)
                t += Time.deltaTime;
            else if (t > 1)
                t = 0;
            if (player)
                player.renderer.material.SetColor("_OutlineColor", Color.Lerp(initColor, Color.yellow, t));
            yield return null;
        }
    }


    public override void BonusDesactivate()
    {
        if (player)
        {
            player.agent.speed /= speedMultiplicator;
            StopCoroutine("UpdateColor");
            player.renderer.material.SetColor("_OutlineColor", initColor);
        }
    }
}
