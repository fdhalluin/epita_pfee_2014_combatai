﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Path {

    public List<int> ids;

    public Path(List<int> path)
    {
        ids = path;
    }

    public void highlight(Color color)
    {
        for (int i = 0; i < ids.Count; i++)
        {
            Node curr = Graph.getNode(ids[i]);
            curr.AlgoColor = color;
            curr.highlightAlgo = true;
        }
    }

    public void unhighlight()
    {
        for (int i = 0; i < ids.Count; i++)
        {
            Node curr = Graph.getNode(ids[i]);
            curr.highlightAlgo = false;
        }
    }
}
