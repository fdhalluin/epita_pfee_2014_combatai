﻿using UnityEngine;
using System.Collections;

public class ShieldBonus : Bonus {

    // Use this for initialization
    void Start()
    {
        stopBonus = false;
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
    }

    public override void BonusActivate()
    {
        player.shield = PlayerMovement.maxShield;
    }

    public override void BonusDesactivate()
    {
    }
}
