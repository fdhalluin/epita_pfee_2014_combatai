﻿using UnityEngine;
using System.Collections;

public class InvisibleBonus : Bonus
{

    Color initColor;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
    }

    public override void Hide()
    {
        base.Hide();
        //foreach (Transform child in transform)
        //    child.active = false;
    }

    public override void BonusActivate()
    {
        player.isInvisible = true;
        initColor = player.renderer.material.color;
        StartCoroutine("UpdateTransparency");
    }

    private IEnumerator UpdateTransparency()
    {
        while (true)
        {
            Color oldColor = player.renderer.material.color;
            Color newColor = new Color(oldColor.r, oldColor.g, oldColor.b, (oldColor.a + 0.1f) % 1.0f);
            player.renderer.material.SetColor("_Color", newColor);
            player.renderer.material.SetColor("_MainColor", newColor);
            player.renderer.material.SetColor("_OutlineColor", newColor);

            yield return new WaitForSeconds(0.1f);
        }
    }

    public override void BonusDesactivate()
    {
        if (player)
        {
            player.isInvisible = false;
            StopCoroutine("UpdateTransparency");
            player.renderer.material.SetColor("_Color", initColor);
            player.renderer.material.SetColor("_MainColor", initColor);
            player.renderer.material.SetColor("_OutlineColor", initColor);
        }
    }
}
