﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FallBackText : MonoBehaviour {

    Text text;

	void Start () {
	
	}

    void Update()
    {
        text = GetComponent<Text>();

        switch (PlayerMovement.orderState)
        {
            case PlayerMovement.OrderState.FALL_BACK:
                text.enabled = true;
                break;

            default:
                text.enabled = false;
                break;
        }
    }
}
