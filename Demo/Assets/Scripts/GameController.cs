﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

    public GameObject victoryScreen;
	public GameObject defeatScreen;
	public Text victoryTimeText;
	public Text defeatTimeText;
	public Text victoryBestTimeText;
	public Text defeatBestTimeText;
	public Text currentTimeText;

	public GameObject lifeBar;
    public bool debugAlgoAmbush = false;
    public bool drawRecommendedPath = false;

    public bool drawArrowExit = false;
    public bool drawArrowNextNode = false;
    public bool drawArrowRecommended = false;

    public static GameController instance = null;

    public static float recover_time = 5;
    public static float recover_speed = 0.2f;

	public float startTime = 0;
	public float currentTime = 0;
	public string scoreKey = "bestTime";

    public GameObject shieldBonus;
    public GameObject invisibleBonus;
    public GameObject indestructibleBonus;
    public GameObject speedBonus;

	public bool running = true;

	void Start ()
	{
		startTime = Time.time;
		currentTime = 0;
		currentTimeText.text = string.Format("{0:0}:{1:00}", Mathf.Floor(currentTime / 60f), currentTime % 60);
		victoryScreen.SetActive(false);
		defeatScreen.SetActive(false);
        instance = this;
		running = true;
	}

	public void Win ()
	{
		if (!running)
			return;
		running = false;
		float best = PlayerPrefs.HasKey(scoreKey) ? PlayerPrefs.GetFloat(scoreKey) : float.MaxValue;
		if (currentTime < best)
		{
			best = currentTime;
		}
		PlayerPrefs.SetFloat(scoreKey, best);
        victoryTimeText.text = string.Format("{0:0}:{1:00}", Mathf.Floor(currentTime / 60f), currentTime % 60);
        victoryBestTimeText.text = string.Format("{0:0}:{1:00}", Mathf.Floor(best / 60f), best % 60);
		victoryScreen.SetActive(true);
	}

	public void Lose ()
	{
		if (!running)
			return;
		running = false;
		float best = PlayerPrefs.HasKey(scoreKey) ? PlayerPrefs.GetFloat(scoreKey) : 0;
        if (defeatTimeText)
            defeatTimeText.text = string.Format("{0:0}:{1:00}", Mathf.Floor(currentTime / 60f), currentTime % 60);
        if (defeatBestTimeText)
            defeatBestTimeText.text = string.Format("{0:0}:{1:00}", Mathf.Floor(best / 60f), best % 60);
        if (defeatScreen)
		    defeatScreen.SetActive(true);
	}

	void Update ()
	{
		if (running)
		{
        	currentTime = Time.time - startTime;
            double min = Mathf.Floor(currentTime / 60);
            double sec = Mathf.Floor(currentTime % 60);
            currentTimeText.text = string.Format("{0:0}:{1:00}", min, sec);
		}

        if (!Menu.fowS)
            light.enabled = true;
        else
            light.enabled = false;

        drawArrowExit = Menu.exitS;
        drawArrowNextNode = Menu.nextNodeS;
        drawArrowRecommended = Menu.recoNextNodeS;
        drawRecommendedPath = Menu.recopathS;
        debugAlgoAmbush = Menu.ambushS;
	}

    public static bool getDebugAlgoAmbush()
    {
        if (instance)
            return instance.debugAlgoAmbush;
        return false;
    }

    public static bool getDrawRecommendedPath()
    {
        if (instance)
            return instance.drawRecommendedPath;
        return false;
    }

    public static bool getDrawArrowExit()
    {
        if (instance)
            return instance.drawArrowExit;
        return false;
    }

    public static bool getDrawArrowNextNode()
    {
        if (instance)
            return instance.drawArrowNextNode;
        return false;
    }

    public static bool getDrawArrowRecommended()
    {
        if (instance)
            return instance.drawArrowRecommended;
        return false;
    }

    public static GameObject getLifeBar()
    {
        if (instance)
            return instance.lifeBar;
        return null;
    }
}
