﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class GUIgraph : EditorWindow {

    Graph myGraph;

    [MenuItem("Graph Generator/Generate Graph")]
    private static void showEditor()
    {
        EditorWindow.GetWindow<GUIgraph>(false, "Generator");
    }

    void OnGUI()
    {
        myGraph = (Graph)EditorGUILayout.ObjectField("Graph to use :", myGraph, typeof(Graph), true);

        if (GUILayout.Button("Generate graph"))
        {
            for (int i = 0; i < Graph.Nodes.Count; i++)
            {
                Node n = Graph.Nodes[i];
                n.TargetNodes.Clear();
                n.ParentNodes.Clear();
            }

            foreach (Transform child in myGraph.transform)
            {
                foreach (Transform child2 in myGraph.transform)
                {
                    if (child != child2 && child.collider.bounds.Intersects(child2.collider.bounds))
                    {
                        Node curr = Graph.getNode(child.transform);
                        Node currTarget = Graph.getNode(child2.transform);

                        curr.TargetNodes.Add(currTarget);
                        curr.ParentNodes.Add(currTarget);
                    }
                }
            }

            this.Close();
        }

        if (GUILayout.Button("Compute distances"))
        {
            Graph.computeDistances();
            this.Close();
        }
    }
}
#endif