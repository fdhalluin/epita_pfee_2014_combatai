﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public Enemy targetEnemy = null;
    public Ally targetAlly = null;
    public PlayerMovement targetPlayer = null;
    public Vector3 startPos;
    public Vector3 arrivalPos;
    public GameObject objectToDestroy;
    public int count = 0;
    public bool started = false;

	void Start () {
	}
	
	void Update () {
        if (!started)
        {
            if (targetAlly != null)
                arrivalPos = targetAlly.transform.position;
            if (targetPlayer != null)
                arrivalPos = targetPlayer.transform.position;
            started = true;
        }
        transform.Translate((arrivalPos.x - startPos.x) / 12.0f, (arrivalPos.y - startPos.y) / 12.0f, (arrivalPos.z - startPos.z) / 12.0f, Space.World);

        if ((arrivalPos.x - transform.position.x) * (arrivalPos.x - transform.position.x) + (arrivalPos.z - transform.position.z) * (arrivalPos.z - transform.position.z) < 1)
        {
            Destroy(objectToDestroy);
            Destroy(this);
        }
	}
}
