﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
    public float riskSliderValue = 0.0F;
    public float oldHSliderValue = 0.0F;
    public int menuWidth = 100;
    public int toggleHeight = 50;
    public int menuHeight = 100;
    public int entitiesHeight = 100;
    public int debugHeight = 100;
    public int marging = 10;
    public int padding = 0;
    public int buttonHeight = 20;
    public int buttonSpace = 5;

    private bool fow = true;
    private bool bonuses = true;
    private bool ambush = false;
    private bool recopath = false;
    private bool exit = false;
    private bool nextNode = false;
    private bool recoNextNode = false;
    private bool drawMenus = false;

    private string nbEnemies = "15";
    private string nbAllies = "5";
    private string hpEnemies = "50";
    private string atkEnemies = "10";
    private string hpAllies = "50";
    private string atkAllies = "10";
    private string hpPlayer = "50";
    private string hpDelay = "-1";
    private string speedAllies = "-1";
    private string speedEnnemies = "-1";
    private string speedPlayer = "-1";

    public static bool fowS = true;
    public static bool bonusesS = true;
    public static bool ambushS = false;
    public static bool recopathS = false;
    public static bool exitS = true;
    public static bool nextNodeS = false;
    public static bool recoNextNodeS = true;
    public static bool drawMenusS = false;

    public static bool mouseInMenu = false;

    public float aggressivitySliderValue = 0.0F;
    public float oldAggressivitySliderValue = 0.0F;
    public float hpRecoverSliderValue = 0.0F;
    public float hpRecoverOldSliderValue = 0.0F;

    void Start()
    {
        fow = fowS;
        bonuses = bonusesS;
        ambush = ambushS;
        recopath = recopathS;
        exit = exitS;
        nextNode = nextNodeS;
        recoNextNode = recoNextNodeS;
        drawMenus = drawMenusS;

        nbEnemies = EnemyDirector.nbrEnemies.ToString();
        nbAllies = AllyDirector.nbrAlliesS.ToString();
        hpEnemies = EnemyDirector.maxHp.ToString();
        atkEnemies = EnemyDirector.attack.ToString();
        hpAllies = AllyDirector.maxHp.ToString();
        atkAllies = AllyDirector.attack.ToString();
        hpPlayer = PlayerMovement.maxHp.ToString();;
        speedPlayer = PlayerMovement.playerSpeed.ToString();
        speedEnnemies = Enemy.enemySpeed.ToString();
        speedAllies = Ally.allySpeed.ToString();
        hpDelay = GameController.recover_time.ToString();
    }

    void OnGUI()
    {
        fowS = fow;
        bonusesS = bonuses;
        ambushS = ambush;
        recopathS = recopath;
        exitS = exit;
        nextNodeS = nextNode;
        recoNextNodeS = recoNextNode;
        drawMenusS = drawMenus;

        int buttonWidth = menuWidth - 2 * padding;
        int x = marging + padding;
        int y = marging;

        int x1 = marging;
        int x2 = menuWidth + 2 * marging;
        int x3 = 2 * menuWidth + 3 * marging;
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Screen.height - Input.mousePosition.y, Input.mousePosition.z);

        // Menus Toggle
        Rect menusRect = new Rect(marging, marging, 2 * menuWidth, toggleHeight);
        GUI.Box(menusRect, "");
        if (GUI.Button(new Rect(x2 - padding, marging + padding/2, menuWidth - marging - padding/2, toggleHeight - padding), "Restart"))
        {
            Application.LoadLevel(Application.loadedLevel);
            AllyDirector.nbrAlliesS = int.Parse(nbAllies);
            if (AllyDirector.nbrAlliesS > 50)
                AllyDirector.nbrAlliesS = 50;
            EnemyDirector.nbrEnemies = int.Parse(nbEnemies);
            if (EnemyDirector.nbrEnemies > 50)
                EnemyDirector.nbrEnemies = 50;
            EnemyDirector.attack = int.Parse(atkEnemies);
            EnemyDirector.maxHp = int.Parse(hpEnemies);
            AllyDirector.attack = int.Parse(atkAllies);
            AllyDirector.maxHp = int.Parse(hpAllies);
            PlayerMovement.maxHp = int.Parse(hpPlayer);
            PlayerMovement.playerSpeed = float.Parse(speedPlayer);
            if (PlayerMovement.playerSpeed > 6)
                PlayerMovement.playerSpeed = 6;
            Ally.allySpeed = float.Parse(speedAllies);
            if (Ally.allySpeed > 6)
                Ally.allySpeed = 6;
            Enemy.enemySpeed = float.Parse(speedEnnemies);
            if (Enemy.enemySpeed > 6)
                Enemy.enemySpeed = 6;
            GameController.recover_time = float.Parse(hpDelay);
        }

        drawMenus = GUI.Toggle(new Rect(x, marging + buttonSpace, buttonWidth, buttonHeight), drawMenus, " Menus");

        mouseInMenu = menusRect.Contains(mousePos);

        if (!drawMenus)
            return;

        // Menu Box
        // 1
        y = toggleHeight + 2 * marging;
        y += buttonSpace;
        Rect enemiesRect = new Rect(x1, y, menuWidth, menuHeight);
        GUI.Box(enemiesRect, "Enemies");

        GUI.Label(new Rect(x, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "Count: ");
        nbEnemies = GUI.TextField(new Rect(x + 60, y, buttonWidth - 60, buttonHeight), nbEnemies, 25);
        GUI.Label(new Rect(x, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "HP: ");
        hpEnemies = GUI.TextField(new Rect(x + 60, y, buttonWidth - 60, buttonHeight), hpEnemies, 25);
        GUI.Label(new Rect(x, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "ATK: ");
        atkEnemies = GUI.TextField(new Rect(x + 60, y, buttonWidth - 60, buttonHeight), atkEnemies, 25);
        GUI.Label(new Rect(x, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "Speed: ");
        speedEnnemies = GUI.TextField(new Rect(x + 60, y, buttonWidth - 60, buttonHeight), speedEnnemies, 25);
        GUI.Label(new Rect(x, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "Agressivity: " + (int)aggressivitySliderValue + "%");
        aggressivitySliderValue = GUI.HorizontalSlider(new Rect(x, y += buttonHeight, buttonWidth, buttonHeight), EnemyDirector.aggressivity, 0.0F, 100.0F);
        if (aggressivitySliderValue != oldAggressivitySliderValue)
        {
            oldAggressivitySliderValue = aggressivitySliderValue;
            EnemyDirector.aggressivity = (int)aggressivitySliderValue;
        }

        // 2
        y = toggleHeight + 2 * marging;
        y += buttonSpace;
        Rect alliesRect = new Rect(x2, y, menuWidth, menuHeight);
        GUI.Box(alliesRect, "Allies");

        GUI.Label(new Rect(x2 + padding, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "Count: ");
        nbAllies = GUI.TextField(new Rect(x2 + padding + 60, y, buttonWidth - 60, buttonHeight), nbAllies, 25);
        GUI.Label(new Rect(x2 + padding, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "HP: ");
        hpAllies = GUI.TextField(new Rect(x2 + padding + 60, y, buttonWidth - 60, buttonHeight), hpAllies, 25);
        GUI.Label(new Rect(x2 + padding, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "ATK: ");
        atkAllies = GUI.TextField(new Rect(x2 + padding + 60, y, buttonWidth - 60, buttonHeight), atkAllies, 25);
        GUI.Label(new Rect(x2 + padding, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "Speed: ");
        speedAllies = GUI.TextField(new Rect(x2 + padding + 60, y, buttonWidth - 60, buttonHeight), speedAllies, 25);
        GUI.Label(new Rect(x2 + padding, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "Risk level: " + (int)((10 - riskSliderValue) * 10) + "%");
        riskSliderValue = GUI.HorizontalSlider(new Rect(x2 + padding, y += buttonHeight, buttonWidth, buttonHeight), AllyDirector.riskCoef, 10.0F, 0.0F);
        if (riskSliderValue != oldHSliderValue)
        {
            oldHSliderValue = riskSliderValue;
            AllyDirector.riskCoef = riskSliderValue;
        }

        // 3
        y = toggleHeight + 2 * marging;
        y += buttonSpace;
        Rect playerRect = new Rect(x3, y, menuWidth, menuHeight);
        GUI.Box(playerRect, "Player");

        GUI.Label(new Rect(x3 + padding, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "HP: ");
        hpPlayer = GUI.TextField(new Rect(x3 + padding + 60, y, buttonWidth - 60, buttonHeight), hpPlayer, 25);
        GUI.Label(new Rect(x3 + padding, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), "Speed: ");
        speedPlayer = GUI.TextField(new Rect(x3 + padding + 60, y, buttonWidth - 60, buttonHeight), speedPlayer, 25);

        // Entities Menu
        y = toggleHeight + menuHeight + 3 * marging;
        Rect entitiesRect = new Rect(marging, y, 3 * menuWidth + 2 * marging, entitiesHeight);
        GUI.Box(entitiesRect, "Entities");

        GUI.Label(new Rect(x, y += buttonHeight + buttonSpace, 3 * buttonWidth, buttonHeight), "HP recovering delay: ");
        hpDelay = GUI.TextField(new Rect(x2, y, buttonWidth - 60, buttonHeight), hpDelay, 25);
        GUI.Label(new Rect(x, y += buttonHeight + buttonSpace, 3 * buttonWidth, buttonHeight), "Hp recovering speed: " + (int)(hpRecoverSliderValue * 100) + "%");
        hpRecoverSliderValue = GUI.HorizontalSlider(new Rect(x, y += buttonHeight, 3 * buttonWidth, buttonHeight), GameController.recover_speed, 0.0F, 1.0F);
        if (hpRecoverSliderValue != hpRecoverOldSliderValue)
        {
            hpRecoverOldSliderValue = hpRecoverSliderValue;
            GameController.recover_speed = hpRecoverSliderValue;
        }

        // Debug Box
        y = toggleHeight + menuHeight + entitiesHeight + 4 * marging;
        Rect debugRect = new Rect(marging, y, menuWidth, debugHeight);
        GUI.Box(debugRect, "Options");

        fow = GUI.Toggle(new Rect(x, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), fow, " Fog of War");
        bonuses = GUI.Toggle(new Rect(x, y += buttonHeight + buttonSpace, buttonWidth, buttonHeight), bonuses, " Bonuses");
        ambush = GUI.Toggle(new Rect(x, y += buttonHeight, buttonWidth, buttonHeight), ambush, " Ambush points");
        recopath = GUI.Toggle(new Rect(x, y += buttonHeight, buttonWidth, buttonHeight), recopath, " Best path");
        exit = GUI.Toggle(new Rect(x, y += buttonHeight, buttonWidth, buttonHeight), exit, " Exit");
        nextNode = GUI.Toggle(new Rect(x, y += buttonHeight, buttonWidth, buttonHeight), nextNode, " Next point");
        recoNextNode = GUI.Toggle(new Rect(x, y += buttonHeight, buttonWidth, buttonHeight), recoNextNode, " Best next");

        // Mouse in menus
        if (menusRect.Contains(mousePos) ||
            enemiesRect.Contains(mousePos) ||
            alliesRect.Contains(mousePos) ||
            playerRect.Contains(mousePos) ||
            entitiesRect.Contains(mousePos) ||
            debugRect.Contains(mousePos))
            mouseInMenu = true;
        else
            mouseInMenu = false;

    }
}
