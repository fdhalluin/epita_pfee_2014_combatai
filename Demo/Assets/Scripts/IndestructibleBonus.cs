﻿using UnityEngine;
using System.Collections;

public class IndestructibleBonus : Bonus {

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
    }

    public override void BonusActivate()
    {
        player.isIndestructible = true;
        player.playerShield.renderer.enabled = true;
        player.playerShield.light.enabled = true;
    }

    public override void BonusDesactivate()
    {
        if (player)
        {
            player.isIndestructible = false;
            player.playerShield.renderer.enabled = false;
            player.playerShield.light.enabled = false;
        }
    }
}
