﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class Ally : EntityBase
{
    public enum AttackState
    {
        Ready,
        Shooting,
        Reloading
    };

	protected override float HpRatio
	{
		get { return hp / AllyDirector.maxHp; }
	}

    protected override float ShieldRatio
    {
        get { return 0; }
    }

	protected override float RechargeRatio
	{
		get { return 1 - Mathf.Clamp01(recharging / MaxTimeRecharging); }
	}

    //public AllyState state;
    public AttackState attackState;
    public NavMeshAgent agent;

	public Transform player;
	public int distance = 2;

    private float hp = AllyDirector.maxHp;
    private Enemy targetEntity;
    private float MaxTimeRecharging = 2.0f;
    private float recharging;
    private float shooting;
    private List<Enemy> nearbyEntites;
    public List<Enemy> visibleEnemies;
    public Enemy attackingEnemy;
    public float attackingEnemyCountdown;
    private Vector3 origin;
    public Node targetNode = null;
    public Node targetNodeExplore = null;
    public Node currentNode = null;
    private float countRotate;
    private bool clockwise;
    private float exploreCountdown = 0.0f;

    public GameObject bullet;
    public GameObject death;
    private float recovery;
    public static float allySpeed = 4.5f;
    private bool flee;

    //private Bullet MyBullet;
    //private GameObject MyBulletObject;

    public Color currentColor;
    public static Color coverColor = new Color(114.0f / 255.0f, 209.0f / 255.0f, 255.0f / 255.0f); // light blue
    public static Color attackColor = new Color(40.0f / 255.0f, 35.0f / 255.0f, 192.0f / 255.0f); // blue
    public static Color shootColor = new Color(0.0f / 255.0f, 0.0f / 255.0f, 150.0f / 255.0f); // dark blue
    public static Color fleeColor = new Color(150.0f / 255.0f, 249.0f / 255.0f, 255.0f / 255.0f); // white

	void Start ()
	{
        if (!AllyDirector.allAllies.Contains(this))
            AllyDirector.allAllies.Add(this);

		agent = GetComponent<NavMeshAgent>();
        agent.speed = allySpeed;
        agent.updateRotation = true;
        nearbyEntites = new List<Enemy>();
        visibleEnemies = new List<Enemy>();
        setState("Cover");
        blackboard.GetFloatVar("FloatHp").Value = hp;

        attackState = AttackState.Ready;
        targetEntity = null;
        recharging = 0.0f;
        shooting = 0.0f;
        countRotate = 0.0f;
        origin = transform.position;
        clockwise = true;

        attackingEnemy = null;
        attackingEnemyCountdown = 0;

        //renderer.material.shader = Shader.Find("Self-Illumin/Outlined Diffuse");
        renderer.material.shader = Shader.Find("Outlined/Silhouetted Bumped Diffuse");
        recovery = 0;
        currentColor = coverColor;
        flee = false;

		InitBars();
	}

    void OnDestroy()
    {
        if (AllyDirector.allAllies.Contains(this))
            AllyDirector.allAllies.Remove(this);
    }
	
	void Update ()
	{
        recovery -= Time.deltaTime;
        flee = true;
        if (recovery <= 0 && hp < AllyDirector.maxHp)
        {
            flee = false;
            if (state() == "Flee")
                setState("Cover");
            hp += GameController.recover_speed * AllyDirector.maxHp * Time.deltaTime;
        }

        currentNode = Graph.getNearestNode(transform);
        if (currentNode != null)
            currentNode.visitAlly();

        nearbyEntites.Clear();

        var hitColliders = Physics.OverlapSphere(transform.position, PlayerMovement.maxRadius);

        renderer.material.SetColor("_Color", currentColor);

        for (var i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].tag == "Enemy")
                nearbyEntites.Add(hitColliders[i].gameObject.GetComponent<Enemy>());
        }

        IntVar ennemiesBlackBoard = blackboard.GetIntVar("IntEnnemiesInRange");
        ennemiesBlackBoard.Value = nearbyEntites.Count;

        FloatVar hpBlackBoard = blackboard.GetFloatVar("FloatHp");
        hpBlackBoard.Value = hp;
        if (hp <= 0)
		{
            var deathEffect = Instantiate(death, transform.position, transform.rotation) as GameObject;
			var deathParticle = deathEffect.GetComponent<ParticleSystem>();
			if (deathParticle != null)
			{
				deathParticle.startColor = currentColor + Color.white * 0.15f; // Let additive particles merge to white
			}
		}

        if (recharging > 0.0f)
            recharging -= Time.deltaTime;
        if (shooting > 0.0f)
            shooting -= Time.deltaTime;
        if (exploreCountdown > 0.0f)
            exploreCountdown -= Time.deltaTime;
        if (attackingEnemyCountdown > 0.0f && targetEntity == null)
        {
            attackingEnemyCountdown -= Time.deltaTime;
        }
        else if (attackingEnemy != null)
        {
            attackingEnemy = null;
            agent.updateRotation = true;
        }

        if (flee && hp <= 15)
        {
            setState("Flee");
        }

        switch (state())
        {
            case "Cover":
                updateCover();
                break;

            case "Attack":
                currentColor = shootColor;
                updateAttack();
                break;

            case "Flee":
                currentColor = fleeColor;
                updateFlee();
                break;
        }

		// Protip: Do the visual update after all logic update, otherwise visual will lag one frame behind
		UpdateBars();
	}

    public void updateFollow()
    {
        if (player != null)
        {
            agent.SetDestination(player.GetComponent<NavMeshAgent>().destination);
            float dst = Vector3.Distance(player.position, transform.position);
            if (dst < distance)
            {
                agent.SetDestination(transform.position);
            }
        }

        testAttack();
    }

    public void updateCover()
    {
        Vector3 sight;
        float a = 0;
        float dst = 0;
        if (targetNode != null)
            dst = Vector3.Distance((targetNode.transform.position) / 1, transform.position);

        if (player != null)
        {
            if (targetNode != null)
            {
                if (dst > distance)
                {
                    agent.SetDestination((targetNode.transform.position) / 1);
                }
                else if (Random.Range(0.0f, 1.0f) < 0.01f && exploreCountdown <= 0.0f)
                {
                    bool stop = false;
                    for (int i = 0; i < AllyDirector.playerNode.TargetNodes.Count; i++)
                    {
                        if (AllyDirector.playerNode.TargetNodes[i].nearbyEnemiesValue > 0)
                        {
                            stop = true;
                            agent.updateRotation = true;
                            break;
                        }
                    }
                    if (!stop)
                        for (int i = 0; i < currentNode.TargetNodes.Count; i++)
                        {
                            if (currentNode.TargetNodes[i] != AllyDirector.playerNode)
                            {
                                targetNodeExplore = currentNode.TargetNodes[i];
                                exploreCountdown = 3.0f;
                                break;
                            }
                        }
                }
            }

            if (targetEntity == null && attackingEnemy != null)
            {
                rotateToward(transform, attackingEnemy.transform, transform.forward, transform.up);
            }
            else if (dst < distance)
            {
                int max = 0;
                Node dangerNode = null;
                for (int i = 0; i < currentNode.TargetNodes.Count; i++)
                {
                    if (currentNode.TargetNodes[i].nearbyEnemiesValue > max)
                    {
                        max = currentNode.TargetNodes[i].nearbyEnemiesValue;
                        dangerNode = currentNode.TargetNodes[i];
                    }
                }
            }

            if (countRotate <= 0.01f && !agent.updateRotation)
            {
                if (Random.Range(0.0f, 1.0f) < 0.01f)
                {
                    float min = float.MaxValue;
                    Node nodeToLook = null;

                    for (int i = 0; i < currentNode.TargetNodes.Count; i++)
                    {
                        if (currentNode.TargetNodes[i].timeLastSeenAllies < min)
                        {
                            min = currentNode.TargetNodes[i].timeLastSeenAllies;
                            nodeToLook = currentNode.TargetNodes[i];
                        }
                    }
                    nodeToLook.timeLastSeenAllies = Time.time;

                    sight = new Vector3(nodeToLook.transform.position.x - transform.position.x, nodeToLook.transform.position.y - transform.position.y, nodeToLook.transform.position.z - transform.position.z);
                    a = Vector3.Angle(transform.forward, sight);

                    countRotate = a;

                    if (Random.Range(0.0f, 1.0f) < 0.5f)
                        clockwise = true;
                    else
                        clockwise = false;
                }
            }

            if (exploreCountdown > 0.0f && !agent.updateRotation)
            {
                agent.SetDestination(targetNodeExplore.transform.position);
            }
        }

        testAttack();
    }

    public void updateAttack()
    {
        switch (attackState)
        {
            case AttackState.Ready:
                updateReady();
                break;

            case AttackState.Reloading:
                updateReloading();
                break;

            case AttackState.Shooting:
                updateShooting();
                break;
        }
    }

    public void updateReady()
    {
        blackboard.GetGameObjectVar("TargetEntity").Value = null;

        testAttack();

        if (state() == "Attack" && targetEntity != null)
        {
            if (recharging <= 0.0f)
            {
                agent.SetDestination(transform.position);
                targetNode = null;
                targetNodeExplore = null;

                targetEntity.hit(AllyDirector.attack);
                shooting = 0.2f;
                recharging = MaxTimeRecharging + shooting;
                attackState = AttackState.Shooting;

                targetEntity.attackingAlly = this;
                targetEntity.attackingAllyCountdown = 2;

                GameObject MyBulletObject = (GameObject)Instantiate(bullet, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
                Bullet MyBullet = MyBulletObject.GetComponent<Bullet>();
                MyBullet.transform.Rotate(new Vector3(0, 0, 1), 90.0f);
                MyBullet.objectToDestroy = MyBulletObject;

                rotateToward(MyBullet.transform, targetEntity.transform, MyBullet.transform.up, new Vector3(0, 1, 0));

                MyBullet.targetEnemy = targetEntity;
                MyBullet.targetAlly = null;
                MyBullet.targetPlayer = null;
                MyBullet.targetEnemy = null;
                MyBullet.startPos = transform.position;
                MyBullet.arrivalPos = targetEntity.transform.position;

                rotateToward(transform, targetEntity.transform, transform.forward, transform.up);
            }
        }
    }

    public void updateReloading()
    {
        if (recharging <= 0)
        {
            targetEntity = null;
            attackState = AttackState.Ready;
            setState("Cover");
        }
    }

    public void updateShooting()
    {
        if (shooting <= 0)
        {
            targetEntity = null;
            recharging = MaxTimeRecharging;
            attackState = AttackState.Reloading;

            //Destroy(MyBulletObject);
            //MyBulletObject = null;
            //MyBullet = null;
        }
    }

    public void updateBypass()
    {
    }

    public void updateFlee()
    {
        int min = int.MaxValue;
        Node best = null;
        for (int i = 0; i < currentNode.TargetNodes.Count; i++)
        {
            int enemies_count = currentNode.TargetNodes[i].getAdjacentEnemiesCount();
            if (enemies_count < min)
            {
                min = enemies_count;
                best = currentNode.TargetNodes[i];
            }
        }
        if (best != null)
            agent.SetDestination(best.transform.position);
    }

    public void updateWait()
    {
        agent.SetDestination(transform.position);
    }

    public void updateDead()
    {
    }

    public void testAttack()
    {
        bool found = false;
        visibleEnemies.Clear();
        for (int i = 0; i < nearbyEntites.Count; i++)
        {
            if (nearbyEntites[i].tag == "Enemy")
            {
                Vector3 sight = new Vector3(nearbyEntites[i].transform.position.x - transform.position.x, nearbyEntites[i].transform.position.y - transform.position.y, nearbyEntites[i].transform.position.z - transform.position.z);
                float a = Vector3.Angle(transform.forward, sight);
                if (Mathf.Abs(a) < PlayerMovement.angleView / 2)
                {
                    RaycastHit hit;

                    float X = nearbyEntites[i].transform.position.x - transform.position.x;
                    float Y = nearbyEntites[i].transform.position.y - transform.position.y;
                    float Z = nearbyEntites[i].transform.position.z - transform.position.z;

                    float distance = X * X + Y * Y + Z * Z;

                    int layerMask = 1 << LayerMask.NameToLayer("Obstacle");
                    if (!Physics.Raycast(transform.position, (nearbyEntites[i].transform.position - transform.position), out hit, Mathf.Sqrt(distance), layerMask))
                    {
                        if (!found)
                        {
                            blackboard.GetGameObjectVar("TargetEntity").Value = nearbyEntites[i].gameObject;
                            targetEntity = nearbyEntites[i];
                            found = true;
                        }
                        visibleEnemies.Add(nearbyEntites[i]);
                    }
                }
            }
        }

        if (!found)
        {
            blackboard.GetGameObjectVar("TargetEntity").Value = null;
            targetEntity = null;
        }
    }

    public float getHp()
    {
        return hp;
    }

    public void hit(int attack)
    {
        recovery = GameController.recover_time;
        hp -= attack;
        if (hp <= 0)
            hp = 0;
    }

    void setState(string state)
    {
        blackboard.GetStringVar("State").Value = state;
    }

    public string state()
    {
        return blackboard.GetStringVar("State").Value;
    }

    public static void rotateToward(Transform t1, Transform t2, Vector3 axisSight, Vector3 axisRotate)
    {
        Vector3 sight = new Vector3(t2.position.x - t1.position.x, t2.position.y - t1.position.y, t2.position.z - t1.position.z);

        float XnormalizedForward = axisSight.x / axisSight.sqrMagnitude;
        float ZnormalizedForward = axisSight.z / axisSight.sqrMagnitude;

        float XnormalizedTarget = sight.x / sight.sqrMagnitude;
        float ZnormalizedTarget = sight.z / sight.sqrMagnitude;

        float a = Vector3.Angle(axisSight, new Vector3(t2.position.x - t1.position.x, t2.position.y - t1.position.y, t2.position.z - t1.position.z));

        if (XnormalizedForward > 0 && ZnormalizedForward > 0)
        {
            if ((ZnormalizedTarget > 0 && XnormalizedTarget > XnormalizedForward)
                || (ZnormalizedTarget < 0 && XnormalizedTarget > 0)
                || (ZnormalizedTarget < 0 && XnormalizedTarget < 0 && -XnormalizedTarget < XnormalizedForward))
                t1.Rotate(axisRotate, a, Space.World);
            else
                t1.Rotate(axisRotate, -a, Space.World);
        }
        else if (XnormalizedForward < 0 && ZnormalizedForward > 0)
        {
            if ((ZnormalizedTarget > 0 && XnormalizedTarget > XnormalizedForward)
                || (ZnormalizedTarget > 0 && XnormalizedTarget > 0)
                || (ZnormalizedTarget < 0 && XnormalizedTarget > 0 && XnormalizedTarget < -XnormalizedForward))
                t1.Rotate(axisRotate, a, Space.World);
            else
                t1.Rotate(axisRotate, -a, Space.World);
        }
        else if (XnormalizedForward > 0 && ZnormalizedForward < 0)
        {
            if ((ZnormalizedTarget < 0 && XnormalizedTarget < XnormalizedForward)
                || (ZnormalizedTarget < 0 && XnormalizedTarget < 0)
                || (ZnormalizedTarget > 0 && XnormalizedTarget < 0 && -XnormalizedTarget < XnormalizedForward))
                t1.Rotate(axisRotate, a, Space.World);
            else
                t1.Rotate(axisRotate, -a, Space.World);
        }
        else
        {
            if ((ZnormalizedTarget < 0 && XnormalizedTarget < XnormalizedForward)
                || (ZnormalizedTarget > 0 && XnormalizedTarget < 0)
                || (ZnormalizedTarget > 0 && XnormalizedTarget > 0 && XnormalizedTarget < -XnormalizedForward))
                t1.Rotate(axisRotate, a, Space.World);
            else
                t1.Rotate(axisRotate, -a, Space.World);
        }
    }
}