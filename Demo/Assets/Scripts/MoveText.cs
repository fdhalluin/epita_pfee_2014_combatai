﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoveText : MonoBehaviour {

    Text text;

	void Start () {
	
	}

    void Update()
    {
        text = GetComponent<Text>();

        switch (PlayerMovement.orderState)
        {
            case PlayerMovement.OrderState.MOVE:
                text.enabled = true;
                break;

            default:
                text.enabled = false;
                break;
        }
    }
}
