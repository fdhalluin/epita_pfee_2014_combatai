﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathTreeNode {

    public List<PathTreeNode> childs;
    public Node curr;
    public PathTreeNode father;
    public PathTreeNode brother;

    public PathTreeNode(Node n)
    {
        curr = n;
        childs = new List<PathTreeNode>();
        father = null;
        brother = null;
    }

    public PathTreeNode(Node n, PathTreeNode Father)
    {
        curr = n;
        childs = new List<PathTreeNode>();
        father = Father;
        brother = null;
    }

    public PathTreeNode addChild(Node n, PathTreeNode Brother)
    {
        if (!fatherIs(n))
        {
            PathTreeNode newNode = new PathTreeNode(n, this);
            childs.Add(newNode);
            if (Brother != null)
                Brother.brother = newNode;
            return newNode;
        }
        return null;
    }

    public bool fatherIs(Node n)
    {
        PathTreeNode fatherTest = father;

        while (fatherTest != null)
        {
            if (fatherTest.curr == n)
                return true;
            fatherTest = fatherTest.father;
        }
        return false;
    }
}
