﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class AllyWithBlackBoard : StateBehaviour {
    public enum AllyState
    {
        Follow,
        Attack,
        Wait,
        Bypass,
        Flee,
        Dead
    };

    public enum AttackState
    {
        Ready,
        Shooting,
        Reloading
    };

    public AllyState state;
    public AttackState attackState;
    public float allySpeed = 0.0001f;
    NavMeshAgent agent;

    public Transform player;
    public int distance = 2;

    private int hp = 50;
    private Enemy targetEntity;
    private float recharging;
    private float shooting;
    private List<Enemy> nearbyEntites;
    private Vector3 origin;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        nearbyEntites = new List<Enemy>();
        state = AllyState.Follow;
        attackState = AttackState.Ready;
        targetEntity = null;
        recharging = 0.0f;
        shooting = 0.0f;
        origin = transform.position;
    }

    void Update()
    {
        nearbyEntites.Clear();

        var hitColliders = Physics.OverlapSphere(transform.position, PlayerMovement.maxRadius);

        for (var i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].tag == "Enemy" && (hitColliders[i].gameObject.GetComponent<Enemy>()).getHp() > 0)
                nearbyEntites.Add(hitColliders[i].gameObject.GetComponent<Enemy>());
        }

        int a = 2;
        List<Variable> var = blackboard.GetVariables(a.GetType());
        Debug.Log(var.Count);

        if (recharging > 0.0f)
            recharging -= Time.deltaTime;
        if (shooting > 0.0f)
            shooting -= Time.deltaTime;

        if (hp == 0)
        {
            state = AllyState.Dead;
            Destroy(gameObject);
            return;
        }
        else if (hp == 10)
        {
            state = AllyState.Flee;
        }

        switch (state)
        {
            case AllyState.Follow:
                updateFollow();
                break;

            case AllyState.Attack:
                updateAttack();
                break;

            case AllyState.Bypass:
                updateBypass();
                break;

            case AllyState.Flee:
                updateFlee();
                break;

            case AllyState.Wait:
                updateWait();
                break;

            case AllyState.Dead:
                updateDead();
                break;
        }
    }

    public void updateFollow()
    {
        if (player != null)
        {
            agent.SetDestination(player.position);
            float dst = Vector3.Distance(player.position, transform.position);
            if (dst < distance)
            {
                agent.SetDestination(transform.position);
            }
        }

        testAttack();
    }

    public void updateAttack()
    {
        agent.SetDestination(transform.position);

        switch (attackState)
        {
            case AttackState.Ready:
                updateReady();
                break;

            case AttackState.Reloading:
                updateReloading();
                break;

            case AttackState.Shooting:
                updateShooting();
                break;
        }
    }

    public void updateReady()
    {
        state = AllyState.Follow;
        testAttack();
        if (state == AllyState.Attack && targetEntity != null)
        {
            if (recharging <= 0.0f)
            {
                agent.SetDestination(transform.position);
                targetEntity.hit(10);
                shooting = 0.2f;
                attackState = AttackState.Shooting;
            }
        }
    }

    public void updateReloading()
    {
        if (recharging <= 0)
        {
            targetEntity = null;
            attackState = AttackState.Ready;
        }
    }

    public void updateShooting()
    {
        if (shooting <= 0)
        {
            targetEntity = null;
            recharging = 2.0f;
            attackState = AttackState.Reloading;
        }
    }

    public void updateBypass()
    {
    }

    public void updateFlee()
    {
        agent.SetDestination(origin);
        if (Enemy.distance(origin, transform.position) < 5)
        {
            hp = 50;
            state = AllyState.Follow;
        }
    }

    public void updateWait()
    {
        agent.SetDestination(transform.position);
    }

    public void updateDead()
    {
    }

    public void testAttack()
    {
        for (int i = 0; i < nearbyEntites.Count; i++)
        {
            if (nearbyEntites[i].tag == "Enemy")
            {
                Vector3 sight = new Vector3(nearbyEntites[i].transform.position.x - transform.position.x, nearbyEntites[i].transform.position.y - transform.position.y, nearbyEntites[i].transform.position.z - transform.position.z);
                float a = Vector3.Angle(transform.forward, sight);
                if (Mathf.Abs(a) < PlayerMovement.angleView / 2)
                {
                    RaycastHit hit;

                    float X = nearbyEntites[i].transform.position.x - transform.position.x;
                    float Y = nearbyEntites[i].transform.position.y - transform.position.y;
                    float Z = nearbyEntites[i].transform.position.z - transform.position.z;

                    float distance = X * X + Y * Y + Z * Z;

                    int layerMask = 1 << LayerMask.NameToLayer("Obstacle");
                    if (!Physics.Raycast(transform.position, (nearbyEntites[i].transform.position - transform.position), out hit, Mathf.Sqrt(distance), layerMask))
                    {
                        state = AllyState.Attack;
                        targetEntity = nearbyEntites[i];
                        return;
                    }
                }
            }
        }
    }

    public int getHp()
    {
        return hp;
    }

    public void hit(int attack)
    {
        hp -= attack;
        if (hp <= 0)
            hp = 0;
    }
}