﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerMovement : EntityBase
{

    public enum OrderState
    {
        STOP = 0,
        MOVE,
        FALL_BACK
    }

	protected override float HpRatio
	{
		get { return hp / maxHp; }
	}

    protected override float ShieldRatio
    {
        get { return shield / maxShield; }
    }
	
	protected override float RechargeRatio
	{
		get { return 0; }
	}

    public static float angleView = 120.0f;
    public static float maxRadius = 20.0f;

    public static float playerSpeed = 3.5f;
    public Node oldNode = null;
    public NavMeshAgent agent;
    public static float maxHp = 50;
    private float hp;
    public static float maxShield = 50;
    public float shield;
    private Vector3 target;
    public Node currentNode = null;
    private Node targetNode = null;
    public bool isInvisible = false;
    public bool isIndestructible = false;

    public GameObject shieldPref;
    public GameObject playerShield;

    public List<Enemy> visibleEnemies;
    private List<Enemy> nearbyEntites;

    private List<Node> pathToFollow = null;
    public GameObject death;
    public float recovery;
    private Vector3 targetMousePosition = new Vector3(-1,-1,-1);

    private float timeSinceStart;
    private int scoreForGameOver = 60;

    public static OrderState orderState;

    void Start() {
        //    rigidbody.freezeRotation = true;
        agent = GetComponent<NavMeshAgent>();
        agent.speed = playerSpeed;
        currentNode = null;
        oldNode = null;
        pathToFollow = new List<Node>();
        AllyDirector.player = this;
        hp = maxHp;
        shield = 0;

        nearbyEntites = new List<Enemy>();
        visibleEnemies = new List<Enemy>();
        recovery = 0;

        timeSinceStart = 0.0f;
        targetMousePosition = new Vector3(-1, -1, -1);
		InitBars();
        scoreForGameOver = 60;
        playerShield = (GameObject)Instantiate(shieldPref, transform.position, transform.rotation);
        playerShield.renderer.enabled = false;
        playerShield.light.enabled = false;

        orderState = OrderState.STOP;
	}

    void OnDestroy()
    {
		GameController.instance.Lose();
    }

    void Update()
    {
        playerShield.transform.position = transform.position;

        recovery -= Time.deltaTime;
        timeSinceStart += Time.deltaTime;

        if (recovery <= 0 && hp < maxHp)
        {
            hp += GameController.recover_speed * maxHp * Time.deltaTime;
        }

        currentNode = Graph.getNearestNode(transform);
        if (currentNode != null)
            currentNode.visitAlly();
        if (currentNode.isExit)
        {
			GameController.instance.Win();
        }

        if (currentNode != null && oldNode != currentNode && currentNode.minNodePath != null)
        {
            oldNode = currentNode;
            PathCalculator.playerNode = currentNode;
            PathCalculator.getAllPaths(Graph.Nodes, currentNode);
        }

        RaycastHit hit;

        if (Input.GetMouseButtonDown(0) && !Menu.mouseInMenu) {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit)) {

                Node n = Graph.getNearestNode(hit.point.x, hit.point.z);
                if (n == currentNode)
                    targetMousePosition = new Vector3(hit.point.x, 0, hit.point.z);
                else
                    targetMousePosition = new Vector3(-1, -1, -1);

                n.setAsPlayerInterest();
                pathToFollow = Graph.getShortestPath(currentNode, n);
                ArrowNextNode.nextNode = n;
            }
        }

        updateMovement();

		if (hp <= 0)
		{
			var deathEffect = Instantiate(death, transform.position, transform.rotation) as GameObject;
            Destroy(gameObject);
        }

        nearbyEntites.Clear();
        visibleEnemies.Clear();
        var hitColliders = Physics.OverlapSphere(transform.position, PlayerMovement.maxRadius + 5); //the player sees further

        for (var i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].tag == "Enemy")
                nearbyEntites.Add(hitColliders[i].gameObject.GetComponent<Enemy>());
        }
        for (int i = 0; i < nearbyEntites.Count; i++)
        {
            if (nearbyEntites[i].tag == "Enemy")
            {
                //Vector3 sight = new Vector3(nearbyEntites[i].transform.position.x - transform.position.x, nearbyEntites[i].transform.position.y - transform.position.y, nearbyEntites[i].transform.position.z - transform.position.z);
                //float a = Vector3.Angle(transform.forward, sight);
                //if (Mathf.Abs(a) < PlayerMovement.angleView / 2) the player sees at 360°
                {
                    float X = nearbyEntites[i].transform.position.x - transform.position.x;
                    float Y = nearbyEntites[i].transform.position.y - transform.position.y;
                    float Z = nearbyEntites[i].transform.position.z - transform.position.z;

                    float distance = X * X + Y * Y + Z * Z;

                    int layerMask = 1 << LayerMask.NameToLayer("Obstacle");
                    if (!Physics.Raycast(transform.position, (nearbyEntites[i].transform.position - transform.position), out hit, Mathf.Sqrt(distance), layerMask))
                    {
                        visibleEnemies.Add(nearbyEntites[i]);
                    }
                }
            }
        }

		// Protip: Do the visual update after all logic update, otherwise visual will lag one frame behind
		UpdateBars();
        UpdateOrderState();
    }

    public void updateMovement()
    {
        if (pathToFollow.Count > 0)
        {
            targetNode = pathToFollow[0];
            if (targetNode == currentNode)
                pathToFollow.RemoveAt(0);
        }
        else
        {
            ArrowNextNode.nextNode = null;
        }

        if (targetNode != null)
        {
            agent.SetDestination(targetNode.transform.position);
            if (targetMousePosition.x != -1)
            {
                agent.SetDestination(targetMousePosition);
            }
            else
            {
                agent.SetDestination(targetNode.transform.position);
            }
        }
        else if (currentNode != null)
        {
            agent.SetDestination(currentNode.transform.position);
        }
    }

    public void hit(int attack)
    {
        if (isIndestructible)
            return;

        recovery = GameController.recover_time;
        if (shield > 0)
            shield -= attack;
        else
            hp -= attack;
        if (hp <= 0)
            hp = 0;
    }

    public float getHp()
    {
        return hp;
    }

    private void UpdateOrderState()
    {
        orderState = OrderState.STOP;

        if (currentNode != null)
        {
            if (currentNode.nearbyAllies.Count > 0 || ArrowRecommended.recommended.nearbyAllies.Count > 0)
                orderState = OrderState.MOVE;

            int nbEnemies = 0;
            for (int i = 0; i < currentNode.NeighboursNb; i++)
            {
                nbEnemies += currentNode.TargetNodes[i].nearbyEnemies.Count;
            }
            if (nbEnemies >= 2 || hp < maxHp)
                orderState = OrderState.FALL_BACK;
        }
    }
}