﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class PathCalculator : MonoBehaviour {

    public static List<Path> allPaths = new List<Path>();
    public static List<Node> ambushPoints = new List<Node>();
    public static Node playerNode = null;
    public static Node exitNode = null;
    static public int count = 0;
    static public int playerDistanceToExit = 0;
    static public PathCalculator reference;
    static public int maxPathToFind = 150;
    static public bool draw = true;
    public float scaleDraw = 20.0f;

    void Start()
    {
        reference = this;
    }

    public static void getAllPaths(List<Node> graph, Node start)
    {
        reference.getAllPathsInstance(graph, start);
    }

    public static void updateAmbushDrawStatic()
    {
        reference.updateAmbushDraw();
    }

    public void updateAmbushDraw()
    {
        StartCoroutine(updateDrawCoroutine());
    }

    public static IEnumerator updateDrawCoroutine()
    {
        bool stop = false;
        while (!stop)
        {
            stop = true;
            for (int i = 0; i < Graph.Nodes.Count; i++)
            {
                if (Graph.Nodes[i].ambushCountDraw < Graph.Nodes[i].ambushCount)
                {
                    Graph.Nodes[i].ambushCountDraw += 0.5f;
                    stop = false;
                }
                if (Graph.Nodes[i].ambushCountDraw > Graph.Nodes[i].ambushCount)
                {
                    Graph.Nodes[i].ambushCountDraw -= 0.5f;
                    stop = false;
                }
            }
            yield return null;
        }
    }

    public void getAllPathsInstance(List<Node> graph, Node start)
    {
        allPaths.Clear();
        count = 0;

        if (exitNode == null)
        {
            for (int i = 0; i < Graph.Nodes.Count; i++)
                if (Graph.Nodes[i].isExit)
                {
                    exitNode = Graph.Nodes[i];
                    break;
                }
        }

        upddatePlayerDistanceToExit();

        StartCoroutine(goThroughIteratif(start));
    }

    public static IEnumerator goThroughIteratif(Node currentNode)
    {
        PathTree pathTree = new PathTree();
        pathTree.root = new PathTreeNode(currentNode);

        PathTreeNode iteratorVertical = pathTree.root;
        PathTreeNode iteratorHorizontal = pathTree.root;

        while (iteratorVertical != null && allPaths.Count < maxPathToFind)
        {
            PathTreeNode brotherToSet = null;
            while (iteratorHorizontal != null && allPaths.Count < maxPathToFind)
            {
                if (iteratorHorizontal.curr.isExit)
                {
                    AddPathFromTree(iteratorHorizontal);
                    if (allPaths.Count % 3 == 0)
                        yield return null;
                }
                else
                {
                    for (int i = 0; i < iteratorHorizontal.curr.TargetNodes.Count; i++)
                    {
                        PathTreeNode brotherResult = iteratorHorizontal.addChild(iteratorHorizontal.curr.TargetNodes[i], brotherToSet);
                        if (brotherResult != null)
                            brotherToSet = brotherResult;
                    }
                }
                iteratorHorizontal = iteratorHorizontal.brother;
            }

            while (iteratorVertical != null)
            {
                if (iteratorVertical.childs.Count > 0)
                {
                    iteratorVertical = iteratorVertical.childs[0];
                    break;
                }
                iteratorVertical = iteratorVertical.brother;
            }
            iteratorHorizontal = iteratorVertical;
        }

        findAmbushPoints();
        updateAmbushDrawStatic();
        ambushPoints.Sort(CompareAmbushPoints);
    }

    public static int CompareAmbushPoints(Node n1, Node n2)
    {
        if (n1.ambushCount > n2.ambushCount)
            return -1;
        if (n1.ambushCount == n2.ambushCount)
            return 0;
        return 1;
    }

    public static void AddPathFromTree(PathTreeNode n)
    {
        List<int> listed = new List<int>();

        while (n != null)
        {
            listed.Add(n.curr.id);
            n = n.father;
        }
        listed.Reverse();

        allPaths.Add(new Path(listed));
    }

    public static void upddatePlayerDistanceToExit()
    {
        Node curr = playerNode;
        playerDistanceToExit = 0;
        while ( curr != null && !curr.isExit)
        {
            Node minNode = curr.minNodePath;
            curr = minNode;
            playerDistanceToExit++;
        }
    }

    public static void highlightPaths(int i)
    {
        List<Color> colors = new List<Color>();

        colors.Add(Color.yellow);

        int previous = i - 1;
        if (previous < 0)
            previous = allPaths.Count - 1;
        allPaths[previous].unhighlight();
        allPaths[i].highlight(colors[0]);
    }

    public static void findAmbushPoints()
    {
        ambushPoints.Clear();

        for (int y = 0; y < Graph.Nodes.Count; y++)
        {
            Graph.Nodes[y].highlightAmbush = false;
            if (draw)
                Graph.Nodes[y].highlightAlgo = true;
            Graph.Nodes[y].ambushCount = 0;
        }

        for (int i = 0; i < allPaths.Count; i++)
        {
            for (int y = 0; y < allPaths[i].ids.Count; y++)
            {
                Graph.getNode(allPaths[i].ids[y]).ambushCount++;
            }
        }

        for (int y = 0; y < Graph.Nodes.Count; y++)
        {
            bool isAmbush = true;
            for (int w = 0; w < Graph.Nodes[y].TargetNodes.Count; w++)
            {
                if (Graph.Nodes[y].TargetNodes[w].ambushCount >= Graph.Nodes[y].ambushCount
                    && !Graph.Nodes[y].TargetNodes[w].isExit
                    && Graph.Nodes[y].TargetNodes[w] != playerNode)
                {
                    isAmbush = false;
                    break;
                }
            }
            Graph.Nodes[y].highlightAmbush = isAmbush;
            if (isAmbush)
                ambushPoints.Add(Graph.Nodes[y]);
        }

        for (int y = 0; y < Graph.Nodes.Count; y++)
        {
            Graph.Nodes[y].ambushCount *= reference.scaleDraw / (float)allPaths.Count;
        }
    }

    public static List<Node> getFlankPoints(Node node)
    {
        List<Node> nodes = new List<Node>();
        for (int i = 0; i < node.TargetNodes.Count; i++)
        {
            nodes.Add(node.TargetNodes[i]);
        }
        return nodes;
    }
}
