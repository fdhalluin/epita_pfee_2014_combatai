﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Node : MonoBehaviour {

    public float PlayerMovementInterest = 0;

    public List<Node> TargetNodes;
    public List<Node> ParentNodes;

    public List<float> TargetDistances = null;
    public List<float> ParentDistances = null;

    public int NeighboursNb = 0;
    public bool isExit = false;
    public int possibleExit = 0;
    public bool isEntrance = false;

    public float timeLastVisitedAllies = 0.0f;
    private float timeLastVisitedEnemies = 0.0f;

    public float timeLastSeenAllies = 0.0f;

    public float timeHighlight = 0.0f;

    public float distanceToExit;
    public bool highlight = false;
    public bool highlightAlgo = false;
    public bool highlightAmbush = false;
    public bool highlightRecommendedPath = false;

    public Node minNodePath = null;

    public int nearbyEnemiesValue;
    public int nearbyEnemiesNumber;
    public List<Enemy> nearbyEnemies;
    public List<Ally> nearbyAllies;
    public PlayerMovement nearbyPlayer;

    public Color AlgoColor;
    public int id;
    public float ambushCount;
    public float ambushCountDraw;
    public static int ID = 0;

    public GameObject linkBestPath;
    public GameObject MylinkBestPath;

    public GameObject cubeAmbushBlue;
    public GameObject MycubeAmbushBlue;

    public GameObject cubeAmbushYellow;
    public GameObject MycubeAmbushYellow;

    public bool linkDrawn;
    public int initialize;

	void Start () {
        timeLastVisitedAllies = 0.0f;
        timeLastVisitedEnemies = 0.0f;

        minNodePath = null;
        timeHighlight = 0.0f;

        ambushCount = 0;
        ambushCountDraw = 0;

        if (!Graph.Nodes.Contains(this))
        {
            Graph.Nodes.Add(this);
        }
        nearbyEnemies = new List<Enemy>();
        nearbyAllies = new List<Ally>();
        nearbyPlayer = null;
        highlightAlgo = false;
        highlightAmbush = false;
        highlightRecommendedPath = false;
        AlgoColor = Color.blue;
        id = ID;
        if (possibleExit == 1 && AllyDirector.randStart == 3 ||
            //possibleExit == 2 && AllyDirector.randStart == 2 ||
            //possibleExit == 3 && AllyDirector.randStart == 1 ||
            possibleExit == 4 && AllyDirector.randStart == 0)
            isExit = true;
        else
            isExit = false;
        ID++;
        nearbyEnemiesNumber = 0;
        nearbyEnemiesValue = 0;

        Vector3 spawnPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Quaternion spawnRotation = Quaternion.identity;
        if (MylinkBestPath == null)
            MylinkBestPath = (GameObject)Instantiate(linkBestPath, spawnPosition, spawnRotation);
        if (MycubeAmbushBlue == null)
            MycubeAmbushBlue = (GameObject)Instantiate(cubeAmbushBlue, spawnPosition, spawnRotation);
        if (MycubeAmbushYellow == null)
            MycubeAmbushYellow = (GameObject)Instantiate(cubeAmbushYellow, spawnPosition, spawnRotation);

        MylinkBestPath.renderer.enabled = false;
        MycubeAmbushBlue.renderer.enabled = true;
        MycubeAmbushYellow.renderer.enabled = false;

        initialize = 0;
	}

    void OnDestroy()
    {
        if (Graph.Nodes.Contains(this))
        {
            Graph.Nodes.Remove(this);
        }
        if (MylinkBestPath != null)
        {
            DestroyImmediate(MylinkBestPath);
            MylinkBestPath = null;
        }
        if (MycubeAmbushBlue != null)
        {
            DestroyImmediate(MycubeAmbushBlue);
            MycubeAmbushBlue = null;
        }
        if (MycubeAmbushYellow != null)
        {
            DestroyImmediate(MycubeAmbushYellow);
            MycubeAmbushYellow = null;
        }
    }

    public void drawLink(Node nextNode)
    {
        float dist = distanceTo(nextNode);
        if (dist > 0)
        {
            MylinkBestPath.transform.localScale = new Vector3(dist, 0.3f, 0.3f);
            MylinkBestPath.transform.position = new Vector3((transform.position.x + nextNode.transform.position.x) / 2, (transform.position.y + nextNode.transform.position.y) / 2, (transform.position.z + nextNode.transform.position.z) / 2);

            float a = Vector3.Angle(transform.right, new Vector3(nextNode.transform.position.x - transform.position.x, nextNode.transform.position.y - transform.position.y, nextNode.transform.position.z - transform.position.z));
            MylinkBestPath.transform.rotation = Quaternion.AngleAxis(-a, new Vector3(0, 1, 0));
            MylinkBestPath.renderer.enabled = true;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Enemy" && (collider.gameObject.GetComponent<Enemy>()).getHp() > 0 && !nearbyEnemies.Contains(collider.gameObject.GetComponent<Enemy>()))
            nearbyEnemies.Add(collider.gameObject.GetComponent<Enemy>());

        if (collider.tag == "Ally" && (collider.gameObject.GetComponent<Ally>()).getHp() > 0 && !nearbyAllies.Contains(collider.gameObject.GetComponent<Ally>()))
            nearbyAllies.Add(collider.gameObject.GetComponent<Ally>());

        if (collider.tag == "Player" && (collider.gameObject.GetComponent<PlayerMovement>()).getHp() > 0 && !nearbyPlayer == collider.gameObject.GetComponent<PlayerMovement>())
            nearbyPlayer = collider.gameObject.GetComponent<PlayerMovement>();
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Enemy" && (collider.gameObject.GetComponent<Enemy>()).getHp() > 0 && nearbyEnemies.Contains(collider.gameObject.GetComponent<Enemy>()))
            nearbyEnemies.Remove(collider.gameObject.GetComponent<Enemy>());

        if (collider.tag == "Ally" && (collider.gameObject.GetComponent<Ally>()).getHp() > 0 && nearbyAllies.Contains(collider.gameObject.GetComponent<Ally>()))
            nearbyAllies.Remove(collider.gameObject.GetComponent<Ally>());

        if (collider.tag == "Player" && (collider.gameObject.GetComponent<PlayerMovement>()).getHp() > 0 && nearbyPlayer == collider.gameObject.GetComponent<PlayerMovement>())
            nearbyPlayer = null;
    }
	
	void Update () {

        if (initialize == 0)
        {
            Vector3 spawnPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            Quaternion spawnRotation = Quaternion.identity;
            int randBonus = Random.Range(0, 200);
            if (randBonus == 0)
                Instantiate(GameController.instance.shieldBonus, spawnPosition, spawnRotation);
            else if (randBonus == 50)
                Instantiate(GameController.instance.invisibleBonus, spawnPosition, spawnRotation);
            else if (randBonus == 100)
                Instantiate(GameController.instance.indestructibleBonus, spawnPosition, spawnRotation);
            else if (randBonus == 150)
                Instantiate(GameController.instance.speedBonus, spawnPosition, spawnRotation);
            initialize = 1;
        }

        NeighboursNb = TargetNodes.Count;

        if (timeHighlight >= 0.0f)
            timeHighlight--;

        if (highlight && timeHighlight <= 0.0f)
            highlight = false;

        float min = distanceToExit;

        for (int i = 0; i < TargetNodes.Count; i++)
        {
            Node n = TargetNodes[i];
            if (n.distanceToExit < min)
            {
                min = n.distanceToExit;
                minNodePath = n;
            }
        }

        if (GameController.getDebugAlgoAmbush())
        {
            if (highlightAmbush)
            {
                MycubeAmbushBlue.renderer.enabled = false;
                MycubeAmbushYellow.renderer.enabled = true;

                MycubeAmbushYellow.transform.position = transform.position + new Vector3(0, ambushCountDraw / 2, 0);
                MycubeAmbushYellow.transform.localScale = new Vector3(1, ambushCountDraw, 1);
            }
            else if (ambushCountDraw > 0)
            {
                MycubeAmbushBlue.renderer.enabled = true;
                MycubeAmbushYellow.renderer.enabled = false;

                MycubeAmbushBlue.transform.position = transform.position + new Vector3(0, ambushCountDraw / 2, 0);
                MycubeAmbushBlue.transform.localScale = new Vector3(1, ambushCountDraw, 1);
            }
        }
        else
        {
            MycubeAmbushBlue.renderer.enabled = false;
            MycubeAmbushYellow.renderer.enabled = false;
        }
	}

    public void highlightBestPath()
    {
        if (!isExit)
        {
            timeHighlight = 2.0f;
            highlight = true;

            if (minNodePath != null && !minNodePath.isExit)
                minNodePath.highlightBestPath();
        }
    }

    public void visitAlly()
    {
        timeLastVisitedAllies = Time.time;
    }

    public void visitEnemy()
    {
        timeLastVisitedEnemies = Time.time;
    }

    public float getLastVisitedAlly()
    {
        return timeLastVisitedAllies;
    }

    public float getLastVisitedEnemy()
    {
        return timeLastVisitedEnemies;
    }

    public float distanceTo(Node n)
    {
        if (n != null && transform != null)
        {
            float X = n.transform.position.x - transform.position.x;
            float Y = n.transform.position.y - transform.position.y;
            float Z = n.transform.position.z - transform.position.z;

            float distance = X * X + Y * Y + Z * Z;
            return Mathf.Sqrt(distance);
        }
        return 0;
    }

    public int getAdjacentAlliesCount()
    {
        int result = 0;
        for (int i = 0; i < TargetNodes.Count; i++)
            result += TargetNodes[i].nearbyAllies.Count;
        result += nearbyAllies.Count;
        return result;
    }

    public int getAdjacentEnemiesCount()
    {
        int result = 0;
        for (int i = 0; i < TargetNodes.Count; i++)
            result += TargetNodes[i].nearbyEnemies.Count;
        result += nearbyEnemies.Count;
        return result;
    }

    public void setAsPlayerInterest()
    {
        for (int i = 0; i < Graph.Nodes.Count; i++)
            Graph.Nodes[i].PlayerMovementInterest = 0;

        for (int i = 0; i < Graph.Nodes.Count; i++)
        {
            for (int j = 0; j < Graph.Nodes.Count; j++)
            {
                Node n = Graph.Nodes[j];
                float min = 0.0f;
                int best = -1;
                for (int iter = 0; iter < n.TargetNodes.Count; iter++)
                {
                    Node target = n.TargetNodes[iter];
                    if (target == this)
                    {
                        n.PlayerMovementInterest = n.TargetDistances[iter];
                        break;
                    }

                    if (target.PlayerMovementInterest > 0.0001f &&
                        (min < 0.0001f || target.PlayerMovementInterest < min))
                    {
                        min = target.PlayerMovementInterest;
                        best = iter;
                    }
                }
                if (best != -1 && (n.PlayerMovementInterest < 0.0001f || n.PlayerMovementInterest > min + n.TargetDistances[best] + 0.0001f) && n != this)
                    n.PlayerMovementInterest = min + n.TargetDistances[best];
            }
        }
    }

    public void assignEnemyValue(int val)
    {
        if (val <= 0) return;
        if (nearbyEnemiesValue < 200 * val)
            nearbyEnemiesValue = 200 * val;

        val -= 2;
        for (int i = 0; i < TargetNodes.Count; i++)
        {
            TargetNodes[i].assignEnemyValue(val);
        }
    }
}
