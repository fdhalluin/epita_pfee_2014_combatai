//----------------------------------------------
//            Behaviour Machine
// Copyright © 2014 Anderson Campos Cardoso
//----------------------------------------------

using UnityEngine;
using System.Collections;

namespace BehaviourMachine {

    /// <summary>
    /// Wrapper class for the InternalGlobalBlackboard component.
    /// <summary>
    [AddComponentMenu("")]
    public sealed class GlobalBlackboard : InternalGlobalBlackboard {}
}